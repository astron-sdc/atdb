#!/usr/bin/python3
"""
    File name: atdb_service.py
    Authors: Nico Vermaas - Astron
    Date created: 2018-11-24
    Description: The main program that controls all atdb_services.
"""

import os
import sys

import argparse
import time
import logging
import logging.config

from atdb_io import ATDB_IO, DEFAULT_ATDB_HOST, DEFAULT_ALTA_HOST
from service_specification import do_specification
from service_scheduler import do_scheduler
from service_executor import do_executor
from service_data_monitor import do_data_monitor
from service_start_ingest import do_start_ingest
from service_ingest_monitor import do_ingest_monitor, do_check_and_send_quality_to_alta
from service_cleanup import do_cleanup, do_checkup_completed_observations
from service_add_dataproduct import do_add_dataproduct
from service_checkup import do_checkup

# the following imports depend on separate pip installations.
# see https://www.astron.nl/wsrt/wiki/doku.php?id=atdb:atdb_installation
# They are placed in try/except blocks to be able to run atdb_service standalone without these dependencies


from pkg_resources import get_distribution
pkg_version = get_distribution('atdb_services').version

LAST_UPDATE = "24 jan 2020"

# ====================================================================

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('execution time: %r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed

# ------------------------------------------------------------------------------#
#                                Module level functions                         #
# ------------------------------------------------------------------------------#
def exit_with_error(message):
    """
    Exit the code for an error.
    :param message: the message to print.
    """
    print(message)
    sys.exit(-1)


def get_arguments(parser):
    """
    Gets the arguments with which this application is called and returns
    the parsed arguments.
    If a parfile is give as argument, the arguments will be overrided
    The args.parfile need to be an absolute path!
    :param parser: the argument parser.
    :return: Returns the arguments.
    """
    args = parser.parse_args()
    if args.parfile:
        args_file = args.parfile
        if os.path.exists(args_file):
            parse_args_params = ['@' + args_file]
            # First add argument file
            # Now add command-line arguments to allow override of settings from file.
            for arg in sys.argv[1:]:  # Ignore first argument, since it is the path to the python script itself
                parse_args_params.append(arg)
            print(parse_args_params)
            args = parser.parse_args(parse_args_params)
        else:
            raise (Exception("Can not find parameter file " + args_file))
    return args

# ------------------------------------------------------------------------------#
#                                Main                                           #
# ------------------------------------------------------------------------------#

def main():
    """
    The main module.
    """
    try:
        logging_config_file = os.path.join(os.path.dirname(__file__), 'atdb_service_logging.ini')
        logging.config.fileConfig(logging_config_file)
        logger = logging.getLogger('atdb_service')
    except:
        pass

    parser = argparse.ArgumentParser(fromfile_prefix_chars='@')

    # Specification parameters (required)
    parser.add_argument("--field_name",
                        default="unknown",
                        help="SPECIFICATION parameter")
    parser.add_argument("--field_ra",
                        default=None,
                        help="SPECIFICATION parameter. Right Ascension in decimal degrees like 202.4842")
    parser.add_argument("--field_ha",
                        default=None,
                        help="SPECIFICATION parameter. Hour Angle in decimal degrees like 202.4842")
    parser.add_argument("--field_dec",
                        default=None,
                        help="SPECIFICATION parameter. Declination in decimal degrees like 47.2306")
    parser.add_argument("--field_beam",
                        default="0",
                        help="SPECIFICATION parameter. Which beam the source should be in")
    parser.add_argument("--integration_factor",
                        default="0",
                        help="SPECIFICATION parameter. Multipled by 1.024 to get seconds")
    parser.add_argument("--central_frequency",
                        default="0",
                        help="SPECIFICATION parameter. In MHz")
    # Specification or Scheduler parameters (required)
    parser.add_argument("--starttime",
                        default=None,
                        help="SPECIFICATION or SCHEDULING parameter. Format like 2018-02-23 21:03:33")
    parser.add_argument("--endtime",
                        default=None,
                        help="SPECIFICATION or SCHEDULING parameter. Format like 2018-02-23 21:03:33")
    parser.add_argument("--duration",
                        default=None,
                        help="SPECIFICATION or SCHEDULING parameter. Format like 21:03:33 or as an integer in seconds. If endtime is not given, then endtime is calculated, otherwise endtime overrides the duration parameter")
    # Specification and Datamonitor parameters (required)
    parser.add_argument("--data_dir",
                        default=None,
                        help="SPECIFICATION and DATA_MONITOR parameter. This is where EXECUTOR must write the data and DATA_MONITOR will monitor it.")
    # Specification parameters (optional)
    parser.add_argument("--taskid", nargs="?",
                        default=None,
                        help="Optional taskID which can be used instead of '--id' to lookup Observations or Dataproducts.")
    parser.add_argument("--taskid_postfix",
                        nargs="?",
                        default="",
                        help="Optional postfix to be added to generated taskid by SPECIFICATION service (used for pipelines).")
    parser.add_argument("--ndps",
                        default=None,
                        help="(Optional) SPECIFICATION parameter: Number of dataproducts to add. For known beam patterns this is handled by the --pattern argument")
    parser.add_argument("--pattern",
                        default="square_39p1_8bit_37beams",
                        help="SPECIFICATION parameter. Beam pattern used to determine the number of beams/dataproducts to add to the Observation/task")
    parser.add_argument("--irods_coll",
                        default=None,
                        help="(Optional) SPECIFICATION parameter: The relative iRODS collections name")
    parser.add_argument("--parset_location",
                        default='/opt/apertif/share/parsets/parset_start_observation_atdb.template',
                        help="SPECIFICATION parameter. This is where parset template can be found for the EXECUTOR service")
    parser.add_argument("--parset_only",
                        default=False,
                        help="If specified then a parset will be created in the --parset_location directory and the observation will go to 'defined' instead of 'scheduled",
                        action="store_true")
    parser.add_argument("--create_parset_early",
                        default=False,
                        help="If specified then the parset will be created during the specification phase, instead of by the executor just before the starttime. "
                             "Be aware that this also means that you cannot change the specification anymore, unless you manually delete the parset first. "
                             "If you manually delete the parset then the executor service will re-create it just before the starttime.",
                        action="store_true")
    parser.add_argument("--combine_script",
                        default='/home/arts/scripts/combine_obs_sc1_for_atdb.sh',
                        help="EXECUTOR. This is where the combine script can be found for the EXECUTOR service")
    parser.add_argument("--node",
                        default=None,
                        help="ADD_DATAPRODUCT. This indicates on which external machine (arts node) the dataproduct lives")
    parser.add_argument("--observing_mode",
                        default='imaging',
                        help="SPECIFICATION parameter: imaging, imaging_pointing, arts_sc1_timing, arts_sc1_baseband, arts_sc4_record, arts_sc4_survey, arts_sc4_dump,")
    parser.add_argument("--process_type",
                        default='system',
                        help="SPECIFICATION parameter: science_observation, science_driftscan, system_pointing, system_reservation")
    parser.add_argument("--science_mode",
                        default=None,
                        help="SPECIFICATION parameter for ARTS. TAB or IAB")
    parser.add_argument("--skip_auto_ingest",
                        default=False,
                        help="If specified then the Observation will end in the 'completed' state instead of (in)complete, and not be automatically ingested and deleted",
                        action="store_true")
    parser.add_argument("--telescopes",
                        default='23456789ABCD',
                        help="SPECIFICATION parameter. List of telescopes used in the observation like 01234567890ABCD. (do not use commas)")
    parser.add_argument("--beams",
                        default='[0..39]',
                        help="SPECIFICATION parameter. List of beams, example: beams=[1..10,11,12]")
    parser.add_argument("--delay_center_offset",
                        default=None,
                        help="[x,y], where x and y are floats. Optionally add a unit 'deg' (default) or 'rad'. example: delay_center_offset=[0.5,0.0]")
    parser.add_argument("--par_file_name",
                        default='source.par',
                        help="Pulsar parameter file (SC1 only)")
    parser.add_argument("--number_of_bins",
                        default='1024',
                        help="Number of bins in pulsar pulse profile (SC1 only)")
    parser.add_argument("--start_band",
                        default='1',
                        help="Lowest frequency band to use (1--16) (SC1 only)")
    parser.add_argument("--end_band",
                        default='16',
                        help="Highest frequency band to use (1--16) (SC1 only). For ARTS end_band=16 => 6bit mode. For imaging, end_band=16 => 8bit mode, end_band=24 =>6bit mode, ")
    parser.add_argument("--process_triggers",
                        default=False,
                        help="Whether or not to run the ARTS real-time pipeline (SC3/4 only)",
                        action="store_true")
    parser.add_argument("--filename",
                        default=None,
                        help="Filename of dataproduct added to a running observation. Like ARTS190101001_CB01_TAB01.fits")
    # Non-workflow general parameters
    parser.add_argument("--status",
                        default=None,
                        help="status to be set by the set-status operation")
    parser.add_argument("--resource","-r",
                        default='observations',
                        help="observations or dataproducts")
    parser.add_argument("--search_key", "-k",
                        default='taskid:180223003',
                        help="Search key used for various services")
    parser.add_argument("--locality_policy",
                        default='cold_tape',
                        help="Where should the data be eventually stored? 'archive', 'cold_tape', 'cold_disk'")
    parser.add_argument("--max_lifetime_on_disk",
                        default=None,
                        help="TTL in minutes after which the ALTA scheduler starts the cold storage process. If not provided, it will be 90 days for imaging and 1 minute for ARTS")

    # Global parameters (required)
    parser.add_argument("--operation","-o",
                        default="None",
                        help="specification, scheduler, executor, data_monitor, start_ingest, ingest_monitor, cleanup, delete_taskid, change_status, checkup")

    # Global parameters
    parser.add_argument("--query",
                        default=None,
                        help="query to the ATDB REST API, used by the CHECKUP service to limit the search")
    parser.add_argument("--obs_mode_filter",
                        default="",
                        help="This instance of the service only acts on Observations that have this (substring) value in their observing_mode. "
                             "That makes it possible to run multiple instances of a service on different locations. "
                             "For example: executor --obs_mode_filter=ARTS will only execute observations for ARTS without interfering with imaging")
    parser.add_argument("--status_filter",
                        default=None,
                        help="The 'start_ingest' can react to a specific status, like 'valid' or 'valid_priority. Default is None, then the start_ingest service will listen for both 'valid_priority' and 'valid', in that order.")
    parser.add_argument("--host_filter",
                        default="",
                        help="When specified, the service checks if the 'data_location' contains this value and will only execute when that is the case.")
    parser.add_argument("--interval",
                        default=None,
                        help="Polling interval in seconds. When enabled this instance of the program will run in monitoring mode.")
    parser.add_argument("--testmode",
                        default=False,
                        help="Test mode. Writes to ATDB, but does not start/stop observations, does not execute ingests and does not delete data from disk.",
                        action="store_true")
    parser.add_argument("--atdb_host",
                        nargs="?",
                        default=DEFAULT_ATDB_HOST,
                        help="Presets are 'dev', 'vm', 'test', 'acc', 'prod'. Otherwise give a full url like https://atdb.astron.nl/atdb")
    parser.add_argument("--alta_host",
                        nargs="?",
                        default=DEFAULT_ALTA_HOST,
                        help="Presets are 'dev', 'test', 'acc', 'prod'. Otherwise give a full url like https://alta-acc.astron.nl/altapi")
    parser.add_argument("-u","--user",
                        nargs="?",
                        default='vagrant',
                        help="Username.")
    parser.add_argument("-p", "--password",
                        nargs="?",
                        default='vagrant',
                        help="Password.")
    # Global parameters (for info)
    parser.add_argument("-v", "--verbose",
                        default=False,
                        help="More information about atdb_services at run time.",
                        action="store_true")
    parser.add_argument("-v2", "--verbose_deep",
                        default=False,
                        help="More information about atdb_interface at run time.",
                        action="store_true")
    parser.add_argument("--version",
                        default=False,
                        help="Show current version of this program.",
                        action="store_true")
    parser.add_argument("--examples", "-e",
                        default=False,
                        help="Show some examples",
                        action="store_true")
    # All parameters in a file
    parser.add_argument('--parfile',
                        nargs='?',
                        type=str,
                        help='Parameter file')

    args = get_arguments(parser)
    try:
        atdb_service = ATDB_IO(args.atdb_host, args.alta_host, args.user, args.password, args.obs_mode_filter,
                               args.host_filter, args.verbose, args.verbose_deep, args.testmode)

        # don't spam the channel for every added dataproduct
        if (args.operation != 'add_dataproduct'):
            host_filter_text = ''
            if args.host_filter!='':
                host_filter_text = " for host "+ args.host_filter

            obs_mode_filter_text = ''
            if args.obs_mode_filter!='':
                obs_mode_filter_text = " for observing_mode "+ args.obs_mode_filter

            if args.testmode:
                message = 'starting *'+args.operation+'* ' + obs_mode_filter_text + host_filter_text + ' service version '+pkg_version+'... (testmode)'
            else:
                message ='starting *' + args.operation+'* ' + obs_mode_filter_text + host_filter_text + ' service version '+pkg_version+'...'

            atdb_service.report(message)
            atdb_service.send_message_to_atdb_slack_channel(message)

        if (args.examples):

            print('atdb_service.py version = '+ pkg_version + " (last updated " + LAST_UPDATE + ")")
            print('--------------------------------------------------------------')
            print()
            print('--- Examples --- ')
            print()
            print("SPECIFCATION creates an Observation with 37 dataproducts with initial status defined with the given specification parameters. TaskID is automatically generated with addition of an optional '_RAW' postfix")
            print(">atdb_service -o specification --pattern square_39p1_8bit_37beams --field_name M51 --starttime 2018-02-23T21:03:33 --endtime 2018-02-24T08:33:34 --field_ra 202.4842 --field_dec 47.2306 --field_beam 0 --integration_factor 30 --central_frequency=1350.12")
            print()
            print("SCHEDULER sets starttime and endtime and PUTS status on 'scheduled'")
            print(">atdb_service -o scheduler --taskid 180906001_RAW --starttime 2018-09-09T06:30:00 --endtime 2018-09-09T06:35:00")
            print()
            print("EXECUTOR checks for starttime and endtime of 'scheduled' Observations and sets its status accordingly")
            print(">atdb_service -o executor --interval 30")
            print()
            print("Start the DATA_MONITOR and let it check for completed observations every minute")
            print(">atdb_service -o data_monitor --interval 60")
            print()
            print("START_INGEST. This service will look for 'valid' tasks and dataproducts and starts the ingest to ALTA")
            print(">atdb_service -o start_ingest --interval 60")
            print()
            print("Start the INGEST_MONITOR and let it check for finished ingests every minute")
            print(">atdb_service -o ingest_monitor --interval 60")
            print()
            print("Change status of all dataproducts with taskid=180223003 to valid")
            print(">atdb_service -o change_status --resource dataproducts --search_key taskid:180223003_IMG --status valid")
            print()
            print("Change status of 1 dataproduct with id=55 to invalid")
            print(">atdb_service -o change_status --resource dataproducts --search_key id:55 --status invalid")
            print()
            print("Change status of observation with taskid=180223003 to valid")
            print("WARNING: this starts the Ingest of this Observation and all its 'valid' dataproducts ")
            print(">atdb_service -o change_status --resource observations --search_key taskid:180223003_IMG --status valid")
            print()
            print("Delete Observation and DataProducts for taskid 180223003_IMG (from the ATDB database only)")
            print(">atdb_service -o delete_taskid --taskid 180223003_IMG")
            print()
            print("Add a dataproduct to an existing Observation (for arts_sc4)")
            print(">atdb_service -o add_dataproduct --taskid 20190106 --node arts001 --data_dir /data2/output/20190106/2019-01-06-09:50:00.J1810-197/fits/CB00 --filename ARTS20190106_CB00_TAB01.fits")
            print()
            print('--------------------------------------------------------------')
            return

        # --------------------------------------------------------------------------------------------------------
        if (args.version):
            print('--- atdb_service.py version = '+ pkg_version + " (last updated " + LAST_UPDATE + ") ---")
            return

        # --------------------------------------------------------------------------------------------------------
        if (args.operation=='specification'):
            do_specification(atdb_service,
                             taskid=args.taskid,
                             taskid_postfix=args.taskid_postfix,
                             initial_status=args.status,
                             ndps=args.ndps,
                             pattern=args.pattern,
                             field_name=args.field_name,
                             starttime=args.starttime,
                             endtime=args.endtime,
                             duration=args.duration,
                             field_ra=args.field_ra,
                             field_ha=args.field_ha,
                             field_dec=args.field_dec,
                             field_beam=args.field_beam,
                             integration_factor=args.integration_factor,
                             central_frequency=args.central_frequency,
                             data_dir=args.data_dir,
                             irods_coll=args.irods_coll,
                             observing_mode=args.observing_mode,
                             process_type=args.process_type,
                             science_mode=args.science_mode,
                             parset_location=args.parset_location,
                             parset_only=args.parset_only,
                             create_parset_early=args.create_parset_early,
                             skip_auto_ingest=args.skip_auto_ingest,
                             telescopes=args.telescopes,
                             par_file_name=args.par_file_name,
                             number_of_bins=args.number_of_bins,
                             start_band=args.start_band,
                             end_band=args.end_band,
                             process_triggers = args.process_triggers,
                             beams = args.beams,
                             delay_center_offset=args.delay_center_offset,
                             locality_policy=args.locality_policy,
                             max_lifetime_on_disk=args.max_lifetime_on_disk)

        # --------------------------------------------------------------------------------------------------------
        if (args.operation == 'scheduler'):
            do_scheduler(atdb_service, taskid=args.taskid, starttime=args.starttime, endtime=args.endtime)

        # --------------------------------------------------------------------------------------------------------
        def check_dependencies(atdb):
            '''
            Check if the dependencies are met, if not, raise an error in slack
            :param atdb:
            :return:
            '''
            atdb.report('*executor* check dependencies')
            try:
                atdb.report('HOME=' + os.environ['HOME'])
            except:
                atdb.report('HOME not set')

            try:
                atdb.report('PYTHONPATH=' + os.environ['PYTHONPATH'])
            except:
                atdb.report('PYTHONPATH not set')

            try:
                atdb.report('LD_LIBRARY_PATH=' + os.environ['LD_LIBRARY_PATH'])
            except:
                atdb.report('LD_LIBRARY_PATH not set')

            try:
                import pyrap.images as pim
                from lofar.parameterset import parameterset
                from apertif.common.util import locate_file
                from apertif.messaging.send_file import send_and_wait_files
            except Exception as err:
                atdb.report(str(err), "print,slack")


        if (args.operation == 'executor'):

            check_dependencies(atdb_service)

            do_executor(atdb_service)
            if args.interval:
                print('*executor* starting polling ' + atdb_service.host + ' every ' + args.interval + ' secs')
                while True:
                    try:
                        time.sleep(int(args.interval))
                        do_executor(atdb_service)
                    except:
                        print('*** executor crashed! ***')
                        print(sys.exc_info()[0])
                        print('trying to continue...')
                        atdb_service.send_message_to_atdb_slack_channel("*executor service* crashed! ... restarting.")

        # --------------------------------------------------------------------------------------------------------
        if (args.operation == 'data_monitor'):
            do_data_monitor(atdb_service)
            if args.interval:
                print('*data_monitor* starting polling ' + atdb_service.host + ' every ' + args.interval + ' secs')
                while True:
                    try:
                        time.sleep(int(args.interval))
                        do_data_monitor(atdb_service)
                    except:
                        print('*** data_monitor crashed! ***')
                        print(sys.exc_info()[0])
                        print('trying to continue...')
                        atdb_service.send_message_to_atdb_slack_channel("*data_monitor service* crashed! ... restarting.")

        # --------------------------------------------------------------------------------------------------------
        if (args.operation == 'start_ingest'):
            # the 'complete' status is triggered by the 'data_monitor' service
            do_start_ingest(atdb_service, args.status_filter)

            if args.interval:
                atdb_service.report('*start_ingest* for status_filter = '+str(args.status_filter)+', starting polling ' + atdb_service.host + ' every ' + args.interval + ' secs')
                while True:
                    try:
                        time.sleep(int(args.interval))
                        do_start_ingest(atdb_service, args.status_filter)
                    except:
                        print('*** start_ingest crashed! ***')
                        print(sys.exc_info()[0])
                        print('trying to continue...')
                        atdb_service.send_message_to_atdb_slack_channel("*start_ingest* crashed! ... restarting.")

        # --------------------------------------------------------------------------------------------------------
        if (args.operation == 'ingest_monitor'):
            do_check_and_send_quality_to_alta(atdb_service)
            do_ingest_monitor(atdb_service)

            if args.interval:
                print('*ingest_monitor* starting polling ' + atdb_service.host + ' every ' + args.interval + ' secs')
                while True:
                    try:
                        time.sleep(int(args.interval))
                        do_check_and_send_quality_to_alta(atdb_service)
                        do_ingest_monitor(atdb_service)

                    except:
                        print('*** ingest_monitor crashed! ***')
                        print(sys.exc_info()[0])
                        print('trying to continue...')
                        atdb_service.send_message_to_atdb_slack_channel("*ingest_monitor service* crashed! ... restarting.")

        # --------------------------------------------------------------------------------------------------------
        if (args.operation == 'cleanup'):
            # the 'archived' status is written by the 'ingest_monitor' service
            # or the 'removing' status is set from the GUI or otherwise.

            do_cleanup(atdb_service,'archived','removed')
            do_cleanup(atdb_service,'removing', 'removed (manual)')
            do_cleanup(atdb_service, 'cleanup', 'removed (manual)')

            # flag completed validation observation as 'cleanup' after 48 hours
            query = "my_status__icontains=completed&process_type__icontains=validation"
            do_checkup_completed_observations(atdb_service, args, query, 48)

            # flag all completed system observations as 'cleanup' after 72 hours
            query = "my_status__icontains=completed&science_observation=false"
            do_checkup_completed_observations(atdb_service, args, query, 72)

            if args.interval:
                print('*cleanup* starting polling ' + atdb_service.host + ' every ' + args.interval + ' secs')
                while True:
                    try:
                        time.sleep(int(args.interval))
                        do_cleanup(atdb_service, 'archived', 'removed')
                        do_cleanup(atdb_service, 'removing', 'removed (manual)')
                        do_cleanup(atdb_service, 'cleanup', 'removed (manual)')

                        # flag completed validation observation as 'cleanup' after 48 hours
                        query = "my_status__icontains=completed&process_type__icontains=validation"
                        do_checkup_completed_observations(atdb_service, args, query, 48)

                        # flag all completed system observations as 'cleanup' after 72 hours
                        query = "my_status__icontains=completed&science_observation=false"
                        do_checkup_completed_observations(atdb_service, args, query, 72)

                    except:
                        print('*** cleanup crashed! ***')
                        print(sys.exc_info()[0])
                        print('trying to continue...')
                        atdb_service.send_message_to_atdb_slack_channel("*cleanup service* crashed! ... restarting.")

        # --------------------------------------------------------------------------------------------------------
        if (args.operation == 'add_dataproduct'):
            do_add_dataproduct(atdb_service, taskid=args.taskid, node=args.node, data_dir=args.data_dir, filename=args.filename)

        # --------------------------------------------------------------------------------------------------------
        if (args.operation=='change_status'):
            atdb_service.do_change_status(resource=args.resource, search_key=args.search_key, status=args.status)

        # --------------------------------------------------------------------------------------------------------
        if (args.operation=='delete_taskid'):
            atdb_service.do_delete_taskid(taskid=args.taskid)

        # --------------------------------------------------------------------------------------------------------
        if (args.operation=='checkup'):
            do_checkup(atdb_service, args)


    except Exception as exp:
        message = str(exp)
        atdb_service.send_message_to_atdb_slack_channel(message)
        exit_with_error(str(exp))

    sys.exit(0)


if __name__ == "__main__":
    main()


#!/usr/bin/python3
"""
    File name: atdb_ingest.py
    Author: Roy de Goei - Astron
    Date created: 2018-09-18
    Date last modified: 2018-09-25
    Description: A python wrapper module for the ATDB start_ingest service
                 It uses the alta_core_utils.session module to execute the alta_ingest
"""

import os
import shutil

from alta_core_utils.session import Session


ATDB_INGEST_TEMP_BASE_FOLDER = os.path.expanduser("~/atdb_client/ingests")


# Vanuit python3 een python 2 (ingest) command starten
ALTA_INGEST_COMMAND = 'alta_ingest'
ALTA_INGEST_DEPLOY_DIRECTORY = os.path.join(os.path.expanduser('~'), 'alta-client')
ALTA_INGEST_DEPLOY_VIRTUAL_ENV = os.path.join(ALTA_INGEST_DEPLOY_DIRECTORY, '.env')
ALTA_INGEST_DEPLOY_BIN = os.path.join(ALTA_INGEST_DEPLOY_VIRTUAL_ENV, 'bin')
ALTA_INGEST_DEPLOY_LIB = os.path.join(ALTA_INGEST_DEPLOY_VIRTUAL_ENV, 'lib')
ALTA_INGEST_DEPLOY_ACTIVATE = os.path.join(ALTA_INGEST_DEPLOY_BIN, 'activate')
# Use verbose
ALTA_INGEST_EXTRA_OPTIONS = "-v"

SECONDS_IN_A_DAY = 86400


# ------------------------------------------------------------------------------#
#                                Module level functions                         #
# ------------------------------------------------------------------------------#
def execute_alta_ingest(taskID, list_of_valid_files, total_size, irods_collection,
                        irods_host, locality_policy, max_lifetime_on_disk,
                        files_are_remote=False, verbose=False):
    """
    Execute an ALTA Ingest which performs:
       - Prepare: Create symbolic link folder and parameter file for alta_ingest
       - The alta_ingest using the parfile as input
       - Post: Remove the symbolic link folder and parameter file
    :param taskID: The taskid of the dataproducts to be ingested
    :param list_of_valid_files: List of files (full path name) with the 'valid' state
    :param total_size: The total size of the ingested dataproducts required for 'reservation'
    :param irods_collection: The relative irods location where to store (inc. taskID)
    :param irods_host: The irods (icat) host name
    :param locality_policy: Storage policy; archive, cold_disk or cold_tape
    :param max_lifetime_on_disk: Time in minutes how long the observation stays in ALTA archive
    :param files_are_remote: if this flag is true then create a .remote file
    Note: The irods host name will be removed if alta_ingest does not need it as parameter anymore
    :return: ret_code: indicated OK (0) else ERROR
             ret_string: formatted ingest output string with stdout and stderr
    """
    if len(list_of_valid_files) == 0:
        ret_code = 0
        ret_string = "Empty list of valid files, so nothing to ingest."
    else:
        try:
            __atdb_ingest_verbose_print(verbose, "Check preconditions")
            check_preconditions()
            # Make sure there are no duplicates in list of files
            list_of_valid_files = sorted(set(list_of_valid_files))

            ingest_folder, ingest_parfile = \
                prepare_ingest(taskID, list_of_valid_files, total_size, irods_collection,
                               irods_host, locality_policy, max_lifetime_on_disk, files_are_remote)
            __atdb_ingest_verbose_print(verbose, "Ingest folder %s and parameter file %s created " %
                                        (ingest_folder, ingest_parfile))

            options = '--parfile %s %s' % (ingest_parfile, ALTA_INGEST_EXTRA_OPTIONS)
            __atdb_ingest_verbose_print(verbose, "Start execute ingest with options " + str(options))
            ingest_output = BaseIngest.execute_ingest(options)
            __atdb_ingest_verbose_print(verbose, "Finish execute ingest")
            formatted_ingest_output = BaseIngest.format_ingest_output(ingest_output)

            __atdb_ingest_verbose_print(verbose, "Cleanup Ingest folder %s and parameter file %s" %
                                        (ingest_folder, ingest_parfile))
            ingest_cleanup(ingest_folder, ingest_parfile)

            ret_code = ingest_output[0]
            ret_string = formatted_ingest_output
        except Exception as e:
            ret_code = -1
            ret_string = str(e)

    return ret_code, ret_string


def check_preconditions():
    """
    Check if the the preconditions of the alta_ingest are met:
     - Check if the environment variable LD_LIBRARY_PATH is set
#     - Check that "/home/vagrant/alta-client/.env/lib is in environment variable LD_LIBRARY_PATH
    If not met, raise exception with advice string
    """
    advice_str = "\nPlease execute export LD_LIBRARY_PATH=%s:/opt/apertif/lib:$LD_LIBRARY_PATH" % ALTA_INGEST_DEPLOY_LIB
    try:
        env_ld_path = os.environ['LD_LIBRARY_PATH']
    except KeyError as e:
        raise Exception("The environment variable LD_LIBRARY_PATH is not set." + advice_str)
#    if ALTA_INGEST_DEPLOY_LIB not in env_ld_path:
#        raise Exception("%s is not in environment variable LD_LIBRARY_PATH %s" % (ALTA_INGEST_DEPLOY_LIB, advice_str) )


def prepare_ingest(taskID, list_of_valid_files, total_size, irods_collection, irods_host, locality_policy,
                   max_lifetime_on_disk, files_are_remote):
    """
    Prepare the ingest. Create a temporary ingest folder
    where all symbolic links are stored. These symbolic links are linked
    to the original data files which has the status valid.
    The temporary ingest folder is  ~/atdb_client/ingests/ingest-[taskId]
    Also create an alta_ingest parameter file in ~/atdb_client/ingests/ingest-[taskid].parfile
    which has the parameters
    --localDir=
    --irodsColl=
    --size=
    --host= (will be removed, requires alta_ingest update)
    So you will get for example when taskID is 180420002:
     'ln -s /data/apertif/WSRTA180420002_B000.MS ~/atdb_client/ingests/ingest-18042002/WSRTA180420002_B000.MS'
     'ln -s /data/apertif/WSRTA180420002_B001.MS ~/atdb_client/ingests/ingest-18042002/WSRTA180420002_B001.MS'
     etc...
    And ~/atdb_client/ingests/ingest-18042002.parfile
    :return: ingest_folder: Name of the temporary ingest folder
             ingest_parfile: Name of the created ingest parameterfile
    """
    ingest_folder = ATDB_INGEST_TEMP_BASE_FOLDER + "/ingest-" + taskID
    ingest_parfile = ATDB_INGEST_TEMP_BASE_FOLDER + "/ingest-" + taskID + ".parfile"
    remote_dataproducts = ingest_folder + "/ingest-" + taskID + ".remote"
    # Always remove folder if exist
    if os.path.exists(ingest_folder):
        shutil.rmtree(ingest_folder)
    # Create folder (recursively)
    os.makedirs(ingest_folder)

    if files_are_remote:
        # create ingest-<taskId>.remote file containing the remote machine, directory and filename

        with open(remote_dataproducts, "w+") as f:
            #print('remote file : '+remote_dataproducts)
            for remote_filename in list_of_valid_files:
                f.write(remote_filename+"\n")
    else:
        # Create symbolic links when files are not remote
        for full_file in list_of_valid_files:
            # Get filename from full path
            file_name = full_file.rsplit('/', 1)[1]
            linked_file = ingest_folder + "/" + file_name
            os.symlink(full_file, linked_file)

    # Create parameterfile (overwrite if exist)
    with open(ingest_parfile, "w+") as f:
        #print('parameter file : ' + ingest_parfile)
        f.write(("--localDir=%s\n" % ingest_folder))
        f.write(("--irodsColl=%s\n" % irods_collection))
        f.write(("--size=%s\n" % str(total_size)))
        f.write(("--host=%s\n" % str(irods_host)))
        f.write(("--ttl=%s\n" % str(max_lifetime_on_disk)))
        f.write(("--locality_policy=%s\n" % str(locality_policy)))

    return ingest_folder, ingest_parfile


def ingest_cleanup(ingest_folder, ingest_parfile):
    """
    Remove the symbolic link folder and the parameterfile.
    :param: ingest_folder: Name of the temporary ingest folder
    :param  ingest_parfile: Name of the created ingest parameterfile
    """
    if os.path.exists(ingest_folder):
        shutil.rmtree(ingest_folder)
    if os.path.exists(ingest_parfile):
        os.remove(ingest_parfile)


def __atdb_ingest_verbose_print(verbose, info_str):
    """
    Print info string if verbose is enabled (default False)
    :param info_str: String to print
    """
    if verbose:
        print("atdb_ingest " + info_str)

# ------------------------------------------------------------------------------#
#                Base Ingest use the alta session                               #
# ------------------------------------------------------------------------------#
class BaseIngest():
    """
    The Alta Base Ingest class.
    Adapted from the irods_integration_tester.py
    """

    # ---------------------------------------------------------------------------#
    @staticmethod
    def create_command(options):
        """
        Creates an ingest command with the provided options.
        :param options: ingest command options
        :return: The command string to be executed from shell.
        """
        alta_ingest = os.path.join(ALTA_INGEST_DEPLOY_BIN, ALTA_INGEST_COMMAND)
        ingest_command = '. ' + ALTA_INGEST_DEPLOY_ACTIVATE + '; ' + alta_ingest + ' %s' % options
        return ingest_command

    # ---------------------------------------------------------------------------#
    @staticmethod
    def execute_ingest(options, timeout_in_seconds=SECONDS_IN_A_DAY):
        """
        Executes the ingest command with the options.
        :param options: the options to executes the ingest command.
        :param timeout_in_seconds: ingest timeout in seconds.
        :return A tuple with (returncode, ingest) from the ingest command.
        """
        ingest_command = BaseIngest.create_command(options)
        with Session.from_default() as sess:
            ingest_output = sess.execute(ingest_command, timeout_in_seconds)
        return ingest_output

    # ---------------------------------------------------------------------------#
    @staticmethod
    def format_ingest_output(ingest_output):
        """
        :param ingest_output: The result of the ingest command
        :return: formatted_ingest_output: A tuple with (returncode, ingest) from the ingest command.
             returncode indicated OK else ERROR
        """
        formatted_ingest_output = "\n ALTA ingest process stdout: \n" + ingest_output[1] \
                                  + "\n ALTA ingest process stderr: \n" + ingest_output[2]
        return formatted_ingest_output

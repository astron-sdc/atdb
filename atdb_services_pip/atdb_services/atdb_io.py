"""
    File name: atdb_io.py
    Authors: Nico Vermaas, Roy de Goei - Astron
    Date created: 2018-11-24
    Date last modified: 2018-12-02
    Description: This is the IO class of the atdb_services that handles the communication with ATDB and ALTA.
                 It also has functionality to write to slack, stdout and logging.
"""

import os
import subprocess
import datetime
import requests
import logging

logger = logging.getLogger(__name__)

from atdb_interface.atdb_interface import ATDB

try:
    from alta_interface.alta_interface import ALTA
except:
    print("no alta_interface available, install alta_interface when start_ingest service is needed on this machine.")

# The request header
ATDB_HEADER = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'authorization': "Basic YWRtaW46YWRtaW4="
}

# some constants
ATDB_HOST_DEV = "http://localhost:8000/atdb"       # your local development environment with Django webserver
ATDB_HOST_VM = "http://192.168.22.22/atdb"         # your local Ansible/Vagrant setup for testing
ATDB_HOST_ACC = "http://192.168.22.25/atdb"        # your local Ansible/Vagrant acceptance setup
ATDB_HOST_TEST = "http://atdb-test.astron.nl/atdb" # the atdb test environment
ATDB_HOST_PROD = "http://atdb.astron.nl/atdb"      # the atdb production environment.
ATDB_HOST_DOCKER = "http://dop457.astron.nl/atdb"      # the atdb production environment.

ALTA_HOST_DEV = "http://localhost:8000/altapi"
ALTA_HOST_TEST = "http://dop457.astron.nl/altapi"
ALTA_HOST_ACC = "https://alta-acc.astron.nl/altapi"
ALTA_HOST_PROD = "https://alta.astron.nl/altapi"
ALTA_HOST_DOCKER = "http://dop457.astron.nl/altapi"

DEFAULT_ATDB_HOST = ATDB_HOST_TEST
DEFAULT_ALTA_HOST = ALTA_HOST_ACC


def unicode_to_ascii(value, encoding='utf-8'):
    """
    :param value:
    :param encoding: UTF-8
    :return: String
    """
    try:
      # python 2
      if isinstance(value, unicode):
          return value.encode(encoding)
      return value
    except:
      # python 3
        return value

class ATDB_IO:
    """
    This is the IO class of the atdb_services that handles the communication with ATDB and ALTA.
                 It also has functionality to write to slack, stdout and logging.
    """
    TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

    def __init__(self, atdb_host, alta_host='', user='', password='', obs_mode_filter='', host_filter='', verbose=False, verbose_deep=False, testmode=False):
        """
        Constructor.
        :param host: the host name of the backend.
        :param username: The username known in Django Admin.
        :param verbose: more information about atdb_service at runtime.
        :param verbose_deep: more information about underlying atdb_interface at runtime.
        :param testmode: runs the services in testmode if true. Look in services what this means per service.
        """

        # accept some presets to set host to dev, test, acc or prod
        self.host = atdb_host
        if self.host=='dev':
            self.host = ATDB_HOST_DEV
        elif self.host=='vm':
            self.host = ATDB_HOST_VM
        elif self.host=='test':
            self.host = ATDB_HOST_TEST
        elif self.host=='prod':
            self.host = ATDB_HOST_PROD
        elif self.host == 'docker':
            self.host = ATDB_HOST_DOCKER
        elif (not self.host.endswith('/')):
            self.host += '/'

        self.alta_host = alta_host

        if self.alta_host=='dev':
            self.alta_host = ALTA_HOST_DEV
        elif self.alta_host=='test':
            self.alta_host = ALTA_HOST_TEST
        elif self.alta_host=='acc':
            self.alta_host = ALTA_HOST_ACC
        elif self.alta_host=='prod':
            self.alta_host = ALTA_HOST_PROD
        elif self.alta_host=='docker':
            self.alta_host = ALTA_HOST_DOCKER

        elif (not self.alta_host.endswith('/')):
            self.alta_host += '/'

        self.verbose = verbose
        self.verbose_deep = verbose_deep
        self.header = ATDB_HEADER
        self.user = user
        self.password = password
        self.testmode = testmode
        self.obs_mode_filter = obs_mode_filter
        self.host_filter = host_filter
        self.atdb_interface = ATDB(self.host, self.verbose_deep)
        #self.atdb_interface = ATDB(self.host)
        try:
            self.alta_interface = ALTA(self.alta_host,self.user,self.password, self.verbose_deep)
        except:
            print("WARNING: No connection to ALTA could be made on "+self.alta_host+" (valid credentials provided?). Continuing without ALTA connection.")
            print("(Currently, only the 'ingest_monitor' service needs an ALTA connection to check if it can change the status from 'ingesting' to 'archived'. The other services will run fine without the ALTA connection)")

    def verbose_print(self, info_str):
        """
        Print info string if verbose is enabled (default False)
        :param info_str: String to print
        """
        if self.verbose:
            timestamp = datetime.datetime.now().strftime(self.TIME_FORMAT)
            print(str(timestamp)+ ' - '+info_str)

    def get_dir_size(self, start_path='.'):
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path, followlinks=True):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        return total_size

# --- remote ssh commands---------------------------------------------------------------------------------------
    # as found in copy_to_alta.py
    def run_on_node(self, hostname, command, background=True):
        """Run command on an ARTS node. Assumes ssh keys have been set up
            node: nr of node (string or int)
            command: command to run
            background: whether to run ssh in the banode: nr of node (string or int)ckground
        """
        if background:
            ssh_cmd = "ssh {} \"{}\" &".format(hostname, command)
        else:
            ssh_cmd = "ssh {} \"{}\"".format(hostname, command)
        # print("Executing '{}'".format(ssh_cmd))
        return os.system(ssh_cmd)

    # copy a dataproduct from its hardcoded name (like tabA.fits) to the name provided by the atdb specfication service
    def copy_dataproduct_remote(self, node, data_location, from_name, to_name):
        """ copy a dataproduct from its hardcoded name (like tabA.fits) to the name provided by the atdb specfication service
            node: nr of node (string or int)
            data_location: directory on the node where the source file is, and where the target file will be copied.
            from_name: file to copy
            to_name : the new file.
        """
        #print('copy dataproduct ' + node + ':/' + data_location + '/' + from_name + ' to ' + to_name)
        src = data_location + '/' + from_name
        tgt = data_location + '/' + to_name
        cmd = 'cp ' + src + ' ' + tgt

        self.run_on_node(node, cmd, background=False)

    # move/rename a remote dataproduct
    def move_dataproduct_remote(self, node, data_location, from_name, to_name):
        """ move/rename a remote dataproduct
            node: nr of node (string or int)
            data_location: directory on the node where the source file is, and where the target file will be copied.
            from_name: filename to rename
            to_name : new filename
        """
        #print('copy dataproduct ' + node + ':/' + data_location + '/' + from_name + ' to ' + to_name)
        src = data_location + '/' + from_name
        tgt = data_location + '/' + to_name
        cmd = 'mv ' + src + ' ' + tgt

        self.run_on_node(node, cmd, background=False)

    # scp a dataproduct from a node to a local dir
    def scp_dataproduct(self, node, from_location, from_name, to_location, to_name):
        """ secure copy a file from a node to the current machine
            node: nr of node (string or int)
            data_location: directory on the node where the source file is, and where the target file will be copied.
            from_name: file to copy
            to_name : the new file.
        """
        #print('copy dataproduct ' + node + ':/' + data_location + '/' + from_name + ' to ' + to_name)
        src = from_location + '/' + from_name
        tgt = to_location + '/' + to_name
        cmd = 'scp ' + node + ':' + src + ' ' + tgt
        os.system(cmd)

    # check if the specified file exist on the node.
    def check_dataproduct(self, node, data_location, filename):
        """ check if the dataproduct exists on the given location on the node)
            node: nr of node (string or int)
            data_location: directory on the node where the source file is, and where the target file will be copied.
            from_name: file to check
        """
        src = data_location + '/' + filename
        cmd = 'test -s ' + src
        if self.run_on_node(node, cmd, background=False) == 0:
            return True
        else:
            return False

    # returns filesize of a remote file
    def get_filesize_remote(self, node, filepath):
        cmd = 'ssh ' + node + ' stat -Lc%s ' + filepath
        try:
            size = subprocess.check_output(cmd, shell=True)
            size = size.split()[0].decode('utf-8')
            return size
        except Exception as err:
            # file does not exist?
            self.report('ERROR get_filesize_remote of' +filepath)
            return 0

    def remove_dataproduct_remote(self, node, data_location, filename):
        """ remove a dataproduct from the given location on the node)
            node: nr of node (string or int)
            data_location: directory on the node where the source file is, and where the target file will be copied.
            filename: file to remove
        """
        src = data_location + '/' + filename
        cmd = 'rm ' + src
        self.run_on_node(node, cmd, background=False)


    # --------------------------------------------------------------------------------------------------------
    # example: change status of observation with taskid=180223003 to valid.
    # change_status('observations','taskid:180223003','valid'
    def do_change_status(self, resource, search_key, status):

        my_key = resource + ':new_status'   # observations:new_status

        # id:27 or taskid:180223003
        params = search_key.split(':')
        search_field = params[0]            # taskid
        search_value = params[1]            # 180223003

        if search_field=='taskid':
            if my_key.startswith('observations'):
                self.atdb_interface.do_PUT(key=my_key, id=None, taskid=search_value, value=status)
            if  my_key.startswith('dataproducts'):
                self.atdb_interface.do_PUT_LIST(key=my_key, taskid=search_value, value=status)
        else:
            self.atdb_interface.do_PUT(key=my_key, id=search_value, taskid=None, value=status)

# --------------------------------------------------------------------------------------------------------
    def do_delete_taskid(self, taskid):

        # find the observation
        cnt = 0
        id = self.atdb_interface.do_GET_ID(key='observations:taskID', value=taskid)
        while (int(id) > 0):
            cnt += 1
            self.verbose_print('delete observation : ' + str(taskid) + ' (id = ' + str(id) + ')')
            self.atdb_interface.do_DELETE(resource='observations', id=id)
            # check for more
            id = self.atdb_interface.do_GET_ID(key='observations:taskID', value=taskid)
        print(str(cnt) + ' observations with taskID ' + str(taskid) + ' removed')

        cnt = 0
        id = self.atdb_interface.do_GET_ID(key='dataproducts:taskID', value=taskid)
        while (int(id) > 0):
            cnt += 1
            self.verbose_print('delete dataproduct for taskID ' + str(taskid) + ' (id = ' + str(id) + ')')
            self.atdb_interface.do_DELETE(resource='dataproducts', id=id)
            # check for more
            id = self.atdb_interface.do_GET_ID(key='dataproducts:taskID', value=taskid)
        print(str(cnt) + ' dataproducts with taskID ' + str(taskid) + ' removed')


# --------------------------------------------------------------------------------------------------------
    def report(self, message, method='logging'):
        self.verbose_print(message)
        if 'ERROR' in message.upper():
            logging.error(message)
        else:
            logging.info(message)

        if 'print' in method:
            self.verbose_print(message)

        if 'slack' in method:
            self.send_message_to_atdb_slack_channel(message)


    def _send_message_to_alta_channel(self, message_str):
        """
        Send a message to the Slack channel #alta-transfers
        With this channel a notification of Ingest ready (or failed) is given.
        Don't raise an Exception in case of error
        :param message_str: Message String
        """
        try:
            payload = {"text": message_str}
            url = "https://hooks.slack.com/services/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"
            res = requests.post(url, data=str(payload))
            self.verbose_print("Result of sending message to alta-channel: " + str(res))
        except Exception as err:
            self.verbose_print("Exception of sending message to alta-channel: " + str(err))


    def send_message_to_atdb_slack_channel(self, message_str):
        """
        Send a message to the Slack channel #atdb-logging
        With this channel a notification of Ingest ready (or failed) is given.
        Don't raise an Exception in case of error
        :param message_str: Message String
        """
        try:
            timestamp = datetime.datetime.now().strftime(self.TIME_FORMAT)
            message_str = unicode_to_ascii(message_str)
            payload = {"text": str(timestamp) + ' - ' + message_str}

            if (self.host == ATDB_HOST_PROD):
                # in production send to 'atdb_logging'
               url = "https://hooks.slack.com/services/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"
            else:
                # otherwise send to 'atdb-test'
               url = "https://hooks.slack.com/services/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"


            res = requests.post(url, data=str(payload))
        except Exception as err:
            print("Error sending message to slack: " + str(err))
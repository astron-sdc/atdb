"""
    File name: service_specification.py
    Author: Nico Vermaas - Astron
    Date created: 2018-11-23
     Description: (Manual) specification service to specify both imaging and ARTS observations.
"""

import os
import datetime
import time
import json
import logging

TASKID_TIME_FORMAT = "%Y%m%d"
DURATION_TIME_FORMAT ="%H:%M:%S"
SPECIFICATION_TIME_FORMAT = "%Y-%m-%d %H:%M:%S"

ALTA_IRODS_DEFAULT_INGEST_PATH="apertif_main/visibilities_default/"

# --- Helper Functions ---------------------------------------------------------------------------------------------

# decorator to time the execution of a function
def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('execution time: %r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed


def get_number_of_dataproducts(atdb, pattern):
    """
    Get the number of dataproducts based on the beam pattern (unless the number is already given
    :param pattern:
    :param ndps:
    :return:
    """

    if pattern == 'ebm_20180720T090400.dat' or \
                    pattern == 'ebm_20180720T064500.dat' or \
                    pattern == 'ebm_20171214T104900.dat' or \
                    pattern == 'hybridXX_20180928_8bit' or \
                    pattern == 'square_39p1_8bit_37beams' or \
                    pattern == 'square_39p1_37beams' or \
                    pattern == 'central_element_beams_x_37beams' or \
                    pattern == 'central_element_beams_y_37beams' or \
                    pattern == 'dev_pass_si62':
        ndps = 37
    elif pattern == 'calibrator_1beam' or \
                    pattern == 'central_element_beam_x' or \
                    pattern == 'central_element_beam_y':
        ndps = 1
    elif pattern == 'testing_3beams':
        ndps = 3
    elif pattern == 'square_39p1_8bit' or \
                    pattern == 'square_39p1' or \
                    pattern == 'central_element_beams_x' or \
                    pattern == 'central_element_beams_y':
        ndps = 40
    else:
        atdb.report(
            "WARNING by *specification*: unsupported beam pattern: " + pattern + '. Provide the --ndps parameter in the specification to indicate the number of dataproducts for this pattern.',
            'print,slack')

    return ndps



@timeit
def generate_taskid(atdb, timestamp, taskid_postfix):
    """
    :param atdb: the atdb interface abstraction layer
    :param timestamp: timestamp on which the taskid is based
    :param taskid_postfix: optional addition to the tasked, like 190405001_IMG
    :return: taskid
    """

    taskid = atdb.atdb_interface.do_GET_NextTaskID(timestamp, taskid_postfix)
    return taskid


def check_starttime(starttime):
    now = datetime.datetime.utcnow()
    target_time = datetime.datetime.strptime(starttime, SPECIFICATION_TIME_FORMAT)

    # Convert to Unix timestamp
    d1_ts = time.mktime(now.timetuple())
    d2_ts = time.mktime(target_time.timetuple())
    if (d1_ts > d2_ts):
        raise (Exception(
            "ERROR by *specification*: starttime must be in the future: "+str(starttime)))


def check_datawriter_host(data_location):
    """
    With the activation of datawriter2 the data_location must always have the format <datawriter>:data_location.
    This function checks that and adds datawriter1 as default when it is missing.
    :param data_location:
    :return: data_location
    """
    try:
        host,path = data_location.split(':')
    except:
        # if no host is given, then add wcudata1 as a default
        host = 'wcudata1:'
        data_location = host+data_location

    return data_location


def calculate_endtime(atdb, starttime, duration):
    """
    if given, use duration to calculate the endtime.
    duration can come in 2 formats. As HH:mm:ss or as an integer in seconds.
    :param atdb:
    :param starttime:
    :param duration:
    :return:
    """

    ds = str(duration)
    if (ds.find(':'))>=0:
        target_time = time.strptime(ds, DURATION_TIME_FORMAT)
        seconds = target_time.tm_hour*3600 + target_time.tm_min*60 + target_time.tm_sec
    else:
        seconds = int(duration)

    if seconds < 0:
        raise (Exception(
            "ERROR by *specification*: 'duration' is negative. WARNING! negative time could cause an FRB!."))


    # Convert to Unix timestamp
    timestamp = datetime.datetime.strptime(starttime, SPECIFICATION_TIME_FORMAT)
    endtime = timestamp + datetime.timedelta(seconds=seconds)
    return endtime


@timeit
def do_create_parset(atdb, parset_location, taskID):
    '''
    Early create the parset file.
    :param atdb: the atdb interface abstraction layer
    :param taskID: the taskID of the observation for which to generated the parset
    :return string: path to generated parset
    '''

    parset_dir, parset_file = os.path.split(parset_location)

    # Get all observation parameters
    observation = atdb.atdb_interface.do_GET_Observation(taskID)

    # Make (blocking) RPC call to parset_rpc service to
    # generate the parset for the specified observation.
    from apertif.messaging.RPC import RPCWrapper
    with RPCWrapper('APERTIF', 'ParsetRPC') as parset_rpc:
        parset_file = parset_rpc.rpc(
            'create_start_observation_parset',
            observation,
            template_path=parset_location,
            parset_dir=parset_dir)

    return parset_file

    # try:
    #     import atdb_parset_generator as atdb_parset_generator
    #     parset_path_atdb = atdb_parset_generator.create_parset_from_template(atdb.atdb_interface, taskID,
    #                                                            template_path=parset_location,
    #                                                            parset_dir=parset_dir)

    #     atdb.report('*specification* : created parset ' + parset_path_atdb, "slack")
    # except Exception as e:
    #     atdb.report("ERROR: No parset created. parset_generator could not be imported: " + str(e),"slack")


# add dataproducts as a batch
@timeit
def add_dataproducts(atdb, taskid, ndps, observing_mode, new_status, field_name):
    """
    add dataproduct as a batch to a given observation
    :param atdb:
    :param taskid: taskid of the observation to which the dataproducts are added
    :param ndps: number of dataproducts to generated (based on beam_pattern)
    :param observing_mode: Only 'Imaging' and 'ARTS_SC1' now still generate dataproducts
    :param new_status: the status of the new dataproducts, this should be 'defined'.
    :param field_name: For SC1 the field_name becomes part of the dataproduct filename
           see ARTS algorithm : https://support.astron.nl/apertif_issuetracker/issues/2508
    :return:
    """
    dps = []
    for i in range(int(ndps)):
        dp = {}
#        if ('ARTS_SC1' in observing_mode.upper()):
#            pulsar = field_name.replace('+','p')
#            pulsar = pulsar.replace('-','m')
#            dp['filename'] = 'ARTS' + str(taskid) + '_' + pulsar + '.ar'
#            dp['new_status'] = new_status

#            atdb.report('adding dataproduct : ' + str(dp['filename']))
#            dps.append(dp)
        if ('IMAGING' in observing_mode.upper()):
            dp['filename'] = 'WSRTA' + str(taskid) + '_B' + str(i).zfill((3)) + '.MS'
            dp['new_status'] = new_status

            atdb.report('adding dataproduct : ' + str(dp['filename']))
            dps.append(dp)

    if len(dps)>0:
        atdb.atdb_interface.do_POST_dataproducts(taskid, dps)
    else:
        atdb.report("*specification* : (intentionally?) no dataproducts created for observing_mode: " + str(observing_mode))



def assign_resources(atdb, observing_mode, data_location, science_observation, taskid):
    """
    This is the precursor of the later 'atdb_reources' service that will handle this logic.
    Currently this is only used to switch between wcudata1 and wcudata2 for certain imaging observations
    :param observing_mode:
    :param process_type:
    :return:
    """

    # only do this for imaging
    if 'IMAGING' in observing_mode.upper():

        # only do it for 'science' observations
        if science_observation:

            try:
                # only do this when there is no host specified in data_location.
                if not ":" in data_location:

                    # check if a taskid is even
                    if (int(taskid) % 2) == 0:
                        # if so, change the host to wcudata2, in all other cases use wcudata1
                        data_location = "wcudata2:" + data_location
                        atdb.report("*specification* : sending "+str(taskid)+" to wcudata2")

            except:
                pass
                # in all other cases, do nothing, 'check_datawriter_host' will catch it and assign wcudata1

        # final check, if data_dir doesn't have a host, use wcudata1
        data_location = check_datawriter_host(data_location)

    return data_location

def check_process_type(observing_mode, process_type):
    """
    process_type is used as a container to drop some extra properties,
    currently: science, system, filler, validation
    They translate to some other settings
    :return:
    """
    science_observation = False
    filler = False

    if "VALIDATION" in process_type.upper():
        science_observation = False

    if "FILLER" in process_type.upper():
        filler = True

    if "POINTING" in observing_mode.upper():
        science_observation = False

    if "SCIENCE" in process_type.upper():
        science_observation = True

    return science_observation,filler

# --- Main Service -------
# ----------------------------------------------------------------------------------------
@timeit
def do_specification(atdb, taskid, taskid_postfix, initial_status, ndps, pattern, field_name, starttime, endtime, duration, field_ra,
                        field_ha, field_dec, field_beam, integration_factor, central_frequency, data_dir, irods_coll,
                     observing_mode, process_type, science_mode, parset_location, parset_only, create_parset_early, skip_auto_ingest,
                     telescopes, par_file_name, number_of_bins, start_band, end_band, process_triggers, beams, delay_center_offset,
                     locality_policy, max_lifetime_on_disk):

    # if no taskid is given, then generate the new taskid based on date and (optional) taskid_postfix
    STATUS_OBS_END = 'scheduled'  # this service will leave the observation in this state
    STATUS_DPS_END = 'defined'  # this service will leave the dataproducts in this state

    if initial_status!=None:
        STATUS_OBS_END = initial_status  # this service will leave the observation in this state
        # STATUS_DPS_END = initial_status  # leave the dataproducts on 'defined', otherwise the data_monitor service will not pick it up

    if taskid is None:
        if starttime!=None:
            target_time = datetime.datetime.strptime(starttime, SPECIFICATION_TIME_FORMAT)
            timestamp = target_time.strftime(TASKID_TIME_FORMAT)
        else:
            timestamp = datetime.datetime.now().strftime(TASKID_TIME_FORMAT)
        taskid = generate_taskid(atdb, timestamp, taskid_postfix)

    # an engineering/test mode to only create a parset without scheduling the observation
    if parset_only:
        STATUS_OBS_END = 'defined'

    # look at the beam pattern to discover how many dataproducts should be created.
    # unless this is overridden by the ndps parameter.
    if ndps is None:
        ndps = get_number_of_dataproducts(atdb,pattern)


    # If not irods collection is given, just use the default (apertif visibilities) one
    if irods_coll is None:
        irods_collection_str = ALTA_IRODS_DEFAULT_INGEST_PATH + str(taskid)
    else:
        if (not irods_coll.endswith('/')):
            irods_coll += '/'
        irods_collection_str = str(irods_coll) + str(taskid)

    # nv: 29 aug 2019: this is now handled by 'assign_resources'
    # check datawriter host (only for imaging modes).
    # if ('IMAGING' in observing_mode.upper()):
    #   data_dir = check_datawriter_host(data_dir)
    #

    # check if the starttime is in the future
    try:
        check_starttime(starttime)
    except:
        raise (Exception(
            "ERROR by *specification*: Invalid starttime."))

    # if given, use duration to calculate the endtime.
    if duration!= None:
        if endtime!=None:
            raise (Exception(
                "ERROR by *specification*: Cannot use both endtime and duration. Please choose one."))
        else:
            endtime = calculate_endtime(atdb, starttime,duration)

    # for ARTS dataproducts, also add the taskID as extra subdirectory to the data_location
    if ('ARTS_SC1' in observing_mode.upper()):
        #data_dir = os.path.join(data_dir, taskid)
        try:
            data_dir = data_dir + '/' + taskid
        except Exception as err:
            atdb.report("ERROR by *specification* : " + taskid + " has wrong or missing 'data_dir'", "print,slack")
            raise (Exception(str(err)))

    # for ARTS_SC4, check if science mode is given
    if ('ARTS_SC4' in observing_mode.upper()):
        if science_mode is None:
            error = "ERROR by *specification* : " + taskid + " missing '--science_mode' (must be TAB or IAB) "
            atdb.report(error, "print,slack")
            raise (Exception(str(error)))

    # translate the values from process_type into flags for the database
    science_observation, filler = check_process_type(observing_mode, process_type)

    # this is the precursor for the later 'atdb_resources' service
    # nv: 1 oct 2019, temporarily disabled because ingest doesn't work with both datawriters active
    data_dir = assign_resources(atdb, observing_mode, data_dir, science_observation, taskid)

    # if max_lifetime_on_disk is not provided then set defaults
    if max_lifetime_on_disk==None:
        if ('ARTS' in observing_mode.upper()):
            # 1 minute
            max_lifetime_on_disk = 60
        else:
            # 90 days (in minutes)
            # max_lifetime_on_disk = 129600
            # 1 weeks (in minutes)
            max_lifetime_on_disk = 20160

    # --- construct payload as json ----------------
    payload = "{"
    payload += '"name" : "' + str(field_name) + '",'
    payload += '"taskID" : "' + str(taskid) + '",'
    payload += '"starttime" : "' + str(starttime) + '",'
    payload += '"endtime" : "' + str(endtime) + '",'
    payload += '"task_type" : "observation",'
    payload += '"beamPattern" : "' + str(pattern) + '",'
    payload += '"field_name" : "' + str(field_name) + '",'

    if field_ra!=None:
        payload += '"field_ra" : "' + str(field_ra) + '",'
    else:
        # nv: 29 apr 2019. For now, while using the old parset_generator still, field_ra needs a value.
        # so, for driftscans (which has field_ha filled in), still write that value to field_ra also
        payload += '"field_ra" : "' + str(field_ha) + '",'

    if field_ha!=None:
        # occasionally there are imaging_pointing observations, which are only technical observations to
        # recalibrate the telescopes and do not create scientific output. They are not pointed at a ra,dec,
        # but at a position relative to the horizon. (hour angle).
        # This is specified as 'field_ha', but (lazily) stored in the same field_ra as normal observations
        # (so, this is not a typo but on purpose).
        #payload += "field_ra=" + str(field_ha) + ','

        # But on 15 april 2019 this behaviour has changed, because the temporary situation has become permanent
        # by calling it 'drift scans'. The backend has been changed to include a field_ha database field.
        payload += '"field_ha" : "' + str(field_ha) + '",'

    payload += '"field_dec" : "' + str(field_dec) + '",'
    payload += '"field_beam" : "' + str(field_beam) + '",'
    payload += '"integration_factor" : "' + str(integration_factor) + '",'
    payload += '"central_frequency" : "' + str(central_frequency) + '",'
    payload += '"data_location" : "' + str(data_dir) + '",'
    payload += '"irods_collection" : "' + irods_collection_str + '",'
    payload += '"max_lifetime_on_disk" : "' + str(max_lifetime_on_disk) + '",'
    payload += '"locality_policy" : "' + str(locality_policy) + '",'
    payload += '"parset_location" : "' + str(parset_location) + '",'
    payload += '"observing_mode" : "' + str(observing_mode) + '",'
    payload += '"process_type" : "' + str(process_type) + '",'
    payload += '"science_mode" : "' + str(science_mode) + '",'
    payload += '"skip_auto_ingest" : "' + str(skip_auto_ingest) + '",'
    payload += '"science_observation" : "' + str(science_observation) + '",'
    payload += '"filler" : "' + str(filler) + '",'
    payload += '"telescopes" : "' + str(telescopes) + '",'
    payload += '"par_file_name" : "' + str(par_file_name) + '",'
    payload += '"number_of_bins" : "' + str(number_of_bins) + '",'
    payload += '"start_band" : "' + str(start_band) + '",'
    payload += '"end_band" : "' + str(end_band) + '",'
    payload += '"process_triggers" : "' + str(process_triggers) + '",'
    if delay_center_offset!=None:
        payload += '"delay_center_offset" : "' + str(delay_center_offset) + '",'

    payload += '"beams" : "' + str(beams) + '",'
    payload += '"new_status" : "defined"'
    payload += "}"


    atdb.report('adding observation : ' + str(taskid))

    try:
        atdb.atdb_interface.do_POST_json(resource='observations', payload=payload)

    except Exception as err:
        atdb.report("ERROR by *specification* : " + taskid + " has specification error...","print,slack")
        raise (Exception(str(err)))


    # add dataproducts
    add_dataproducts(atdb, taskid, ndps, observing_mode, STATUS_DPS_END, field_name)


    # optional parameter to create a parset during specification time instead of near execution time
    # nv: 12 apr 2019, The 'create_parset_early' functionality will be removed when the RPC calls to generate the
    #     parset are in place. Because it is really undesirable behaviour to have the parset created this early.
    if (parset_only or create_parset_early):
        do_create_parset(atdb, parset_location, taskid)

    # everything is done for this new observation, put its status to 'scheduled'
    atdb.atdb_interface.do_PUT(key='observations:new_status', id=None, taskid=taskid, value=STATUS_OBS_END)
    atdb.report("*specification* :" + taskid + " " + STATUS_OBS_END, "print,slack")

    print(taskid)

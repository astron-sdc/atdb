"""
    File name: parset_generator.py
    Author: Boudewijn Hut - Astron
    Date created: 2018-09-18
    Date last modified: 2019-01-09
    Description: A python wrapper module for the parset generation
                 It uses modules from the apertif repo
                 Should be used for Apertif as well Arts project                 
"""
# First 'load' the casacore, This will avoid the following error
# ImportError: /usr/lib/libcasa_tables.so.2: undefined symbol: _ZN4casa11MutexedInitC1EPFvPvES1_NS_5Mutex4TypeE
# during import of from apertif.tools.add_beam_dirs_to_parset import add_beam_dirs
import pyrap.images as pim

from lofar.parameterset import parameterset
from apertif.common.util import locate_file
from apertif.tools.add_beam_dirs_to_parset import add_beam_dirs

print('** parset_generator (atdb version) **')

skip_pointing = False
try:
    from apertif.tools.add_pointing_pattern_to_parset import add_pointing_pattern
except Exception as e:
    # nv: 25jan2018 very temporary solution to prevent an import error until the next Apertif rollout
    skip_pointing = True
    print(
        "WARNING: apertif.tools.add_beam_dirs_to_parset not yet deployed. Continuing without imaging_pointing functionality.")

from apertif.tools.frequency_conversion import centralFrequency2LOFrequencies
from apertif.messaging.send_file import send_and_wait_files
from datetime import datetime, timedelta
from iso8601 import parse_date


PARSET_TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
MAX_TIME_SLEW = 10 * 60  # maximum time required to slew the dishes, in seconds

# ------------------------------------------------------------------------------#
#                                Module level functions                         #
# ------------------------------------------------------------------------------#


def send_parset_to_bus(parset_path, timeout=20.0):
    """
    Send parset to the bus and wait for response
    :param parset_path: path to the parset file
    :param timeout: number of seconds to wait for responses from controllers (opt)
    :return: reply: reply from the bus for this parset file
    """
    reply = send_and_wait_files(parset_path, timeout=timeout)
    reply = reply[parset_path]
    return reply


def create_parset_from_template(atdb_interface, taskID, template_path, parset_dir):
    """
    Create valid parset for an observation, based on a template and the provided taskID
       - Create a parset for this taskID
       - Add parset keys based on the observation specification in the database
       - Returns the path to the generated parset
    :param atdb_interface: an ATDB instance on which do_GET() can be called
    :param taskID: ID of the task for which a parset is generated
    :param template_path: path to the parset template
    :param parset_dir: path to the directory that should hold the new parset file
    
    :return: parset_path: full path to the generated parset
    """
    #parset_path = get_string('%s/%s.parset' % (parset_dir, taskID))
    parset_path = get_string('%s/%s.parset' % (parset_dir, taskID))
    # create parset by copying the template and by filling in the taskID
    template_path = get_string(template_path)
    parset = parameterset(template_path)
    parset.replace('task.taskID', get_string(taskID))
    parset.writeFile(get_string(parset_path))
    # add information from the database
    add_parset_keys_without_conversion(parset_path, atdb_interface)
    add_parset_keys_with_conversion(parset_path, atdb_interface)
    # make sure the parset becomes valid (i.e. passing the checks in obsIbletInfo)
    obs_mode = str(atdb_interface.do_GET(key='observations:observing_mode', id=None, taskid=taskID) )
    # fill in other keys
    add_observing_mode(parset_path, obs_mode)
    add_recipient_list(parset_path, obs_mode=obs_mode)
    add_beam_dirs(parset_path)

    # add pointing pattern for imaging_pointing observations
    if not skip_pointing:
        if 'imaging_pointing' in obs_mode:
            add_pointing_pattern(parset_path)

    # Add file names to parset, retrieve all dataproducts for taskid
    if 'imaging' in obs_mode:
        ids = atdb_interface.do_GET_LIST(key='dataproducts:id', query='taskID=' + taskID)
        filenames = []
        for id in ids:
            filename = atdb_interface.do_GET(key='dataproducts:filename', id=id, taskid=None)
            filenames.append(filename)
        # Make sure start with beam 0
        filenames.sort()
        add_ms_names(parset_path, filenames)
    return parset_path


def add_observing_mode(parset_path, obs_mode_str):
    """
    :param parset_path: path to the parset that should have the keys added
    :param obs_mode_str: string of observing mode
    Add observer mode to the parset
    """
    parset = parameterset(get_string(parset_path))
    parset.replace('task.observingMode', get_string(obs_mode_str))
    parset.writeFile(get_string(parset_path))


def add_parset_keys_without_conversion(parset_path, atdb_interface):
    """
    Add parset keys based on data in the database, without changing the data value
    :param parset_path: path to the parset that should have the keys added
    :param atdb_interface: an ATDB instance on which do_GET() can be called
    """

    # get taskID from parset
    parset = parameterset(get_string(parset_path))
    taskID = parset.getInt("task.taskID")

    # define how db specification keys map to parset keys
    obs_mode = str(atdb_interface.do_GET(key='observations:observing_mode', id=None, taskid=taskID) )
    spec_key_map = {}
    spec_key_map['observations:field_name'] = ['task.source.name', 'task.fieldName']
    spec_key_map['observations:beamPattern'] = 'task.beamSet.0.pattern'
    spec_key_map['observations:field_beam'] = 'task.source.beam'
    spec_key_map['observations:data_location'] = 'dataWriter.outputPath'

    if 'imaging' in obs_mode:
        spec_key_map['observations:integration_factor'] = 'task.timeIntegrationFactor'
    elif 'arts_sc1' in obs_mode:
        spec_key_map['observations:par_file_name'] = 'arts.timing.parFile'
        spec_key_map['observations:integration_factor'] = 'arts.timing.integrationTime'
        spec_key_map['observations:number_of_bins'] = 'arts.timing.bins'
    # loop over db keys

    for spec_key in spec_key_map.keys():
        parset_key = spec_key_map[spec_key]
        if atdb_interface is None:
            # define valid value if no interface available
            parset_val = str(0)
            if parset_key is 'task.beamSet.0.pattern':
                parset_val = 'square_39p1_8bit_37beams'
        else:
            # retrieve data from database
            parset_val = str(atdb_interface.do_GET(key=spec_key, id=None, taskid=taskID) )
        # add data to the parset
        if not isinstance(parset_key, list):
            parset.replace(get_string(parset_key), get_string(parset_val))
        else:
            parset_keys = parset_key
            for parset_key in parset_keys:
                parset.replace(get_string(parset_key), get_string(parset_val))
    parset.writeFile(get_string(parset_path))


def add_parset_keys_with_conversion(parset_path, atdb_interface):
    """
    Add parset keys based on data in the database, with conversion to system settings
    :param parset_path: path to the parset that should have the keys added
    :param atdb_interface: an ATDB instance on which do_GET() can be called
    """
    # get taskID from parset
    parset = parameterset(parset_path)
    taskID = parset.getInt ("task.taskID")

    # define which function to use for conversion of what db specification key
    # key_func_spec_map - key: parset key
    #                   - first value in list: function name that does conversion
    #                   - other values in list: key for database for which value will
    #                     be passed as argument to the function
    obs_mode = str(atdb_interface.do_GET(key='observations:observing_mode', id=None, taskid=taskID) )
    key_func_spec_map = {}
    key_func_spec_map['task.LO1freq'] = [determineLO1Frequency, 'observations:central_frequency']
    key_func_spec_map['task.beamSet.0.compoundBeams'] = [determineBeamSelection, 'observations:beamPattern']
    key_func_spec_map['task.source.direction'] = [determineSourceDirection, 'observations:field_ra', 'observations:field_dec']
    key_func_spec_map['_control.command.execution_time'] = [determineExecutionTime, 'observations:starttime']
    key_func_spec_map['_control.command.deadline'] = [convertTime, 'observations:endtime']
    key_func_spec_map['task.startTime'] = [convertTime, 'observations:starttime']
    key_func_spec_map['task.stopTime'] = [convertTime, 'observations:endtime']
    key_func_spec_map['task.telescopes'] = [convertTelescopeList, 'observations:telescopes']
    key_func_spec_map['task.beamSet.0.subbands'] = [determineSubbandSelection, 'observations:beamPattern']

    if 'imaging' in obs_mode: # overwrite/add for imaging
        key_func_spec_map['task.beamSet.0.subbands'] = [determineSubbandSelectionImaging, 'observations:start_band', 'observations:end_band']

    if 'arts_sc1' in obs_mode: # overwrite/add for ARTS
        key_func_spec_map['task.beamSet.0.subbands'] = [determineSubbandSelectionARTS, 'observations:start_band', 'observations:end_band']
        key_func_spec_map['arts.timing.recordBaseband'] = [determineTimingRecordBaseband, 'observations:observing_mode']
        key_func_spec_map['arts.timing.timing'] = [determineTimingTiming, 'observations:observing_mode']

    if 'arts_sc4' in obs_mode:  # overwrite/add for ARTS SC4
        key_func_spec_map['task.beamSet.0.subbands'] = [determineSubbandSelectionARTS, 'observations:start_band',
                                                        'observations:end_band']

    # loop over parset keys
    for parset_key in key_func_spec_map.keys():
        # selection conversion method
        conversionMethod = key_func_spec_map[parset_key][0]
        if len(key_func_spec_map[parset_key]) == 1:
            parset_val = conversionMethod()
        else:
            # retrieve data from database
            conversionArguments = []
            for spec_key in key_func_spec_map[parset_key][1:]:
                if atdb_interface == None: # define valid value if no interface available
                    if spec_key is 'observations:central_frequency':
                        conversionArguments.append('1400')
                    elif spec_key is 'observations:beamPattern':
                        conversionArguments.append('square_39p1_8bit_37beams')
                    elif spec_key is 'observations:starttime':
                        conversionArguments.append('2018-09-26 06:00:01')
                    else:
                        conversionArguments.append(str(0))
                else:
                    conversionArguments.append(atdb_interface.do_GET(key=spec_key, id=None, taskid=taskID))
            # do conversion
            parset_val = conversionMethod(*conversionArguments)
        # add data to the parset
        #print('*replace* '+str(parset_key)+ ' =>  '+str(parset_val))
        parset.replace(get_string(parset_key), get_string(parset_val))
    parset.writeFile(get_string(parset_path))


def add_recipient_list(parset_path, obs_mode='imaging'):
    """
    Add recipient list to the parset, addressing specific controllers.
    Some controllers are always added, like DirectionControl@ccu-corr and CorrelatorControl@ccu-corr.
    Based on the telescope selection and the defined datawriter host, SignalControl, DelayCompControl
    and DatawriterControl is added with specific hosts.
    :param parset_path: path to the parset that should have the keys added
    """
    parset = parameterset(get_string(parset_path))
    # define standard controllers, always in a parset
    recipients = ['DirectionControl@ccu-corr']
    # add signal and delayCompControl based on telescope selection in parset
    telescopeList = parset.getStringVector("task.telescopes")
    for tel in telescopeList:
        recipients.append('SignalControl@%s' % tel2hostname(tel))
        recipients.append('DelayCompControl@%s' % tel2hostname(tel))
    # add observation mode dependent controllers
    if 'imaging' in obs_mode:
        # central uniboards
        recipients.append('CorrelatorControl@ccu-corr')
        # datawriter
        datawriterHost = parset.getString("task.beamSet.0.dataWriter.hostname")
        recipients.append('DatawriterControl@%s' % datawriterHost)
    if 'arts_sc1' in obs_mode:
        recipients.append('ARTSTimingControl@arts0')
    # write recipient list to parset
    parset.replace('_msg.recipients', get_string(str(recipients)))
    parset.writeFile(get_string(parset_path))


def add_ms_names(parset_path, filenames):
    """
    Add MS names to the parset
    :param parset_path: The full path name of the parset file
    :param filenames: List of filenames sorted by name
    """
    parset = parameterset(parset_path)
    # define the measurement set for each beam
    idx = 0
    for filename in filenames:
        key = 'dataWriter.beamSet.0.compoundBeam.%d.MSName' % idx
        parset.replace(get_string(key), get_string(filename))
        idx += 1
    parset.writeFile(get_string(parset_path))


def add_fits_names(parset_path, filenames, atdb_interface):
    """
    Add fits file names (ARTS SC4) to the parset
    :param parset_path: The full path name of the parset file
    :param filenames: List of filenames sorted by name
    """
    parset = parameterset(parset_path)
    for filename in filenames:
        id = atdb_interface.do_GET_ID(key='dataproducts:filename', value=filename)
        nodename = atdb_interface.do_GET(key='dataproducts:node', id=id, taskid=None)

        # The filename says something about compound beam and TAB id. This info is also requires in the
        # parset key. Translate this filename to parset_key , which is used in the ranslate the filename to a
        # Also remove the .fits from filename
        # eg. "ARTS200108001_CB38_TAB12.fits" or "ARTS190107009_CB39.fits"
        if 'TAB' in filename:
            compoundbeam_id = _get_substring(filename, 'CB', '_TAB')
            tab_id = _get_substring(filename, '_TAB', '.')
        else:
            compoundbeam_id = _get_substring(filename, 'CB', '.')
            tab_id = '0'

        key = 'arts.dataSet.compoundBeam.%d.TAB.%d.baseName' % (int(compoundbeam_id), int(tab_id))
        filename = nodename + ":" + filename.split('.fit',1)[0]
        parset.replace(get_string(key), get_string(filename))
    parset.writeFile(get_string(parset_path))


def _get_substring(my_str, start_str, end_str):
    """
    Retrieve the substring from the given string between start end string
    :param my_str: The main string
    :param start_str: Start indication string
    :param end_str:  End indication string
    :return: The result string
    """
    start_len = len(start_str)
    sub_str = my_str[my_str.find(start_str)+start_len:my_str.find(end_str)]
    return sub_str


def force_integration_factor_int(parset_path):
    """
    Force the integration factor to be an integer value.
    The database currently (2018-10-03) contains double numbers, that should become integer numbers.
    As long that has not been updated, this function fixes the parset
    """
    parset = parameterset(parset_path)
    key = 'task.timeIntegrationFactor'
    factor_double = parset.getDouble(key)
    parset.replace(get_string(key), get_string(str(int(factor_double))))
    parset.writeFile(get_string(parset_path))


def tel2hostname(tel):
    """ Convert a telescope name ("RT2") to a LCU hostname ("lcu-rt2") """
    return 'lcu-%s' % tel.lower()


def determineLO1Frequency(centralFrequency):
    """
    Determine the LO1 frequency for a given central frequency in MHz
    :param centralFrequency: central frequency in MHz
    """
    if isinstance(centralFrequency, basestring):  # convert from string to float
        centralFrequency = float(centralFrequency)
    (f_lo1, f_lo2) = centralFrequency2LOFrequencies(centralFrequency*1e6)
    f_lo1 = '%i' % (f_lo1/1e6) # convert to string and to MHz
    return f_lo1


def determineBeamSelection(beamPattern):
    """
    Determine the beam selection, based on the beam pattern name and the content of the corresponding cb_offsets
    file
    :param beamPattern: beam pattern identifier, used to find the correct cb_offsets file
    """
    cb_offsets_file = locate_file('%s.cb_offsets' % beamPattern, subdir='share/cb_offsets')
    if cb_offsets_file is None:
        raise ValueError("Pattern file %s not found" % beamPattern)
    cb_offsets = parameterset(get_string(cb_offsets_file))
    n_beam = cb_offsets.getInt("nrOfCompoundBeams")
    if n_beam == 1:
        return '[0]'
    else:
        return '[0..%d]' % (n_beam - 1)

def determineSubbandSelection(beamPattern):
    return '[64..319]' # TODO, when using 6 bit this should become '[64..447]'
	# nv: 7feb2019, this is a hardcoded hack to test 6 bit mode.
    # return '[64..447]'


def determineSubbandSelectionImaging(start_band, end_band):
    """
    Determine the subband selection for imaging
    :param int start_band: number in range [1, 16] of first subband
    :param int end_band: number in range [1, 16] of end subband
    :return string: subband selection in range [64, 447]
    """
    if (end_band==16):
        # 8bit mode
        return '[64..319]'

    elif (end_band==24):
        # 6bit mode
        return '[64..447]'

    else:
        # do the ARTS magic
        first_sb = 64
        n_sb_per_band = 24
        start_sb = first_sb + (start_band - 1) * n_sb_per_band
        end_sb = first_sb + (end_band) * n_sb_per_band - 1
        return '[%i..%i]' % (start_sb, end_sb)


def determineSubbandSelectionARTS(start_band, end_band):
    """ 
    Determine the subband selection for ARTS
    :param int start_band: number in range [1, 16] of first subband
    :param int end_band: number in range [1, 16] of end subband
    :return string: subband selection in range [64, 447]
    """
    first_sb = 64
    n_sb_per_band = 24
    start_sb = first_sb + (start_band-1)*n_sb_per_band
    end_sb = first_sb + (end_band)*n_sb_per_band - 1
    return '[%i..%i]' % (start_sb, end_sb)


def determineTimingRecordBaseband(obs_mode):
    if 'arts_sc1_baseband' in obs_mode:
        return 'True'
    else:
        return 'False'

def determineTimingTiming(obs_mode):
    if 'arts_sc1_timing' in obs_mode:
        return 'True'
    else:
        return 'False'


def determineSourceDirection(ra, dec):
    """
    Determine the source direction by combining the provided field_ra and field_dec coordinates
    :params ra: ra value
    :params dec: dec value
    """
    return '[%sdeg, %sdeg]' % (ra, dec)


def determineExecutionTime(startTime):
    """
    Determine the execution time of this task, assuming that this function is called when the previous
    measurement has finished. This tasks execution time should be at max 10 minutes before the start time,
    in order to allow the dishes to slew to the correct position.
    :param startTime: start time of this task
    """
    execTime = parse_date(startTime) - timedelta(seconds=MAX_TIME_SLEW)
    return execTime.strftime(PARSET_TIME_FORMAT)

def convertTime(isoTime):
    """
    Convert the time string from ATDB which is expresses as (example) "2018-10-03T10:10:04Z"
    to "2018-10-03 10:10:04" which is required by the parset
    :param isoTime: time with format "YYYY-MM-DDThh:mm:ssZ"
    :return: time with format "YYYY-MM-DD hh:mm:ss"
    """
    my_time = parse_date(isoTime)
    return my_time.strftime(PARSET_TIME_FORMAT)


def convertTelescopeList(telescopeString):
    """
    Convert the telescope string to a list of telescopes.
    Fori example: string '23ad' would become ['RT2', 'RT3', 'RTA', 'RTD']
    :param telescopeString: string of telescope characters (single char per RT)
    :return:  list of telescope names
    """
    as_list = [get_string('RT%c' % tel.upper()) for tel in telescopeString]
    as_string = str(as_list)
    as_string = as_string.replace("'",'')
    return as_string


def get_string(str_or_unicode, encoding='utf-8'):
    """
    Check if parameter is unicode or string. If unicode convert to string
    otherwise just return the string.
    The parameters of the parameterset class -> in C++ PyParameterSet class
    expect std::string and not unicode otherwise it will raise an
    exception "did not match C++ signature"
    So make sure that parameter is a string
    :param str_or_unicode:
    :param encoding: UTF-8
    :return: String
    """
    if isinstance(str_or_unicode, unicode):
        return str_or_unicode.encode(encoding)
    return str_or_unicode


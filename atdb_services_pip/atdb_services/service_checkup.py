"""
    File name: service_checkup.py
    Author: Nico Vermaas - Astron
    Date created: 2019-10-22
    Description: An ATDB service to check if the database is still consistant with ALTA and the datawriters
"""

import os
import datetime
import time

def get_age_in_hours(atdb,target_timestring):

    now = datetime.datetime.utcnow()
    target_time = datetime.datetime.strptime(target_timestring, atdb.TIME_FORMAT)

    # Convert to Unix timestamp
    d1_ts = time.mktime(now.timetuple())
    d2_ts = time.mktime(target_time.timetuple())
    age_in_hours = round(((d1_ts - d2_ts) / 3600), 2)

    return age_in_hours

# ------------------------------------------------------------------------------------------------------------------

def checkup_removed_from_datawriter(atdb, args):

    # check if observations are indeed removed from the datawriter

    # build the query to include all statusses with 'removed' in it. This will also search for 'removed (manual)'
    STATUS_TO_CHECK = 'removed'
    query = 'my_status__icontains='+STATUS_TO_CHECK

    # note that this service runs locally on the datawriter, so it only checks observations for this datawriter
    if atdb.host_filter!='':
        query = query + '&data_location__icontains=' + atdb.host_filter

    # possibly an extra --query is given to limit the range of taskid's to check
    if args.query!=None:
        query = query + "&" + args.query

    # gather the list of taskIDs to check
    count = 0
    page = 1
    while page > 0:

        taskIDs, total_count, next_page = atdb.atdb_interface.do_GET_LIST_page(key='observations:taskID',
                                                                               query=query, page=page)

        if total_count > 0:
            atdb.report('*checkup * found '+str(total_count)+ ' observations to check.')

        # loop through the list of 'removed' observations and check if they exists
        for taskID in taskIDs:
            count = count + 1
            atdb.report('checking ' + str(count) + ' of ' + str(total_count))

            data_dir = atdb.atdb_interface.do_GET(key='observations:data_location', id=None, taskid=taskID)

            # split off the host from the location
            # host should now always be onboard...
            try:
                datawriter, data_location = data_dir.split(':')
            except:
                # ... just in case the host is not onboard
                data_location = data_dir

            # every dataproduct is a different directory in the main folder.
            # If one exists, they all exists, so just search for beam 000
            # construct a directory name
            directory = 'WSRTA'+taskID+"_B000.MS"

            try:

                filepath = os.path.join(data_location, directory)
                if os.path.exists(filepath):
                    # uhoh, it is still there. Do not remove, but warn.
                    atdb.report("WARNING: "+taskID+" still exists on disk")


            except Exception as err:

                atdb.report("ERROR by *checkup* : " + str(err),"print,slack")

        page = int(next_page)



def checkup_completed_observations(atdb, args, process_type, age_limit_in_hours):

        # check if observations are indeed removed from the datawriter

        # build the query to include all statusses with 'removed' in it. This will also search for 'removed (manual)'

        query = "my_status__icontains=completed&process_type__icontains="+process_type

        # possibly an extra --query is given to limit the range of taskid's to check
        if args.query != None:
            query = query + "&" + args.query

        # gather the list of taskIDs to check
        count = 0
        page = 1
        while page > 0:

            taskIDs, total_count, next_page = atdb.atdb_interface.do_GET_LIST_page(key='observations:taskID',
                                                                                   query=query, page=page)

            if total_count > 0:
                atdb.report('*checkup * found ' + str(total_count) + ' observations to check.')

            # loop through the list of 'removed' observations and check if they exists
            for taskID in taskIDs:
                count = count + 1
                try:
                    endtime = atdb.atdb_interface.do_GET(key='observations:endtime', id=None, taskid=taskID)

                    age_in_hours = get_age_in_hours(atdb,endtime)

                    if (age_in_hours > age_limit_in_hours):
                        atdb.report(str(count) + " - " + taskID + " age in hours: " + str(age_in_hours) + "... cleanup","slack")
                        atdb.atdb_interface.do_PUT(key='observations:new_status', id=None, taskid=taskID, value="cleanup")

                except Exception as err:

                    atdb.report("ERROR by *checkup* : " + str(err), "print,slack")

            page = int(next_page)


# --------------------------------------------------------------------------------------------------------
def do_checkup(atdb, args):

    # check if removed observations still exist on the current datawriter
    #checkup_removed_from_datawriter(atdb,args)

    # check if there are 'completed' verification observations in ATDB.
    checkup_completed_observations(atdb, args, 'validation', 24)

    # checkup_completed_observations(atdb, args, 'system', 100)
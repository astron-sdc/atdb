"""
    File name: service_ingest_monitor.py
    Author: Nico Vermaas - Astron
    Date created: 2018-11-23
    Description: checks in ALTA if dataproducts are present, which marks a successful ingest.
                 Finally, the status is set to archived
"""

STATUS_START = 'ingesting'          # the statusses that trigger this service.
STATUS_END   = 'archived'           # this service will leave the observation and dataproducts in this state

# --------------------------------------------------------------------------------------------------------
def do_ingest_monitor(atdb):

    # check if observations and dataproducts with status 'ingesting' if they have arrived in ALTA
    # if they do, then put the status on 'archived'.

    # get the list taskID of 'ingesting' observations
    query = 'my_status=' + STATUS_START + '&observing_mode__icontains=' + atdb.obs_mode_filter
    taskIDs = atdb.atdb_interface.do_GET_LIST(key='observations:taskID', query=query)

    if len(taskIDs) > 0:
        atdb.report('*ingest_monitor* found the following ' + STATUS_START + ' tasks: ' + str(taskIDs))

    # connect to ALTA
    for taskID in taskIDs:
        try:
            if not atdb.testmode:
                alta_id = atdb.alta_interface.do_GET_ID(key='activities:runId', value=taskID)
            else:
                # in testmode: fake a succesful query to ALTA
                alta_id = 1
        except Exception as err:
            raise (Exception("ERROR: " + str(err) + ' - perhaps could not reach ALTA at '+ atdb.alta_host))

        if (int(alta_id) > 0):
            atdb.report('Observation ' + taskID + ' found in ALTA = ' + str(alta_id))
            # dataproduct was found in ALTA, put it on 'archived' in ATDB
            atdb.atdb_interface.do_PUT(key='observations:new_status', id=None, taskid=taskID, value=STATUS_END)
            atdb.report("*ingest_monitor* : " + taskID + " " + STATUS_END,"slack")

            if not atdb.testmode:
               # Send a message to the #alta-transfer slack channel
               observing_mode = atdb.atdb_interface.do_GET(key='observations:observing_mode', id=None, runid=taskID)
               message_str = "ATDB: Ingest of taskID=%s (observing mode = %s) to ALTA is finished." % (str(taskID), str(observing_mode))
               atdb._send_message_to_alta_channel(message_str)

    # get the list of names of 'ingesting' dataproducts
    names = atdb.atdb_interface.do_GET_LIST(key='dataproducts:name', query='my_status=' + STATUS_START)

    # connect to ALTA.
    for name in names:
        # get the alta_id of the dataproduct
        try:
            if not atdb.testmode:
                alta_id = atdb.alta_interface.do_GET_ID(key='dataproducts:name', value=name)
            else:
                # in testmode: fake a succesful query to ALTA
                alta_id = 1
        except Exception as err:
            raise (Exception("ERROR: " + str(err) + ' - perhaps could not reach ALTA at '+ atdb.alta_host))

        # atdb.report('Dataproduct found in ALTA = ' + str(alta_id)+' '+str(name))

        # check if this dataproduct has already arrived in ALTA..
        if (int(alta_id) > 0):
            # ... if it has arrived in ALTA then put it on 'archived' in ATDB
            # get the atdb_id of this dataproduct
            atdb_id = atdb.atdb_interface.do_GET_ID(key='dataproducts:name', value=name)
            atdb.report('set status of ' + name + ' to ' + STATUS_END)
            atdb.atdb_interface.do_PUT(key='dataproducts:new_status', id=atdb_id, taskid=None, value=STATUS_END)


def handle_quality(atdb,check_quality,write_quality):

    query = "quality=" + check_quality
    taskIDs = atdb.atdb_interface.do_GET_LIST(key='observations:taskID', query=query)

    if len(taskIDs) > 0:
        atdb.report('*ingest_monitor* found the following ' + query + ' tasks: ' + str(taskIDs))

    # connect to ALTA
    for taskID in taskIDs:
        # check if alta can be reached
        alta_id = atdb.alta_interface.do_GET_ID(key='activities:runId', value=taskID)
        if (int(alta_id) > 0):

            # write the quality to both ATDB and ALTA
            atdb.alta_interface.do_PUT(key='observations:quality', id=None, runid=taskID, value=write_quality)
            atdb.atdb_interface.do_PUT(key='observations:quality', id=None, taskid=taskID, value=write_quality)
            atdb.report("*ingest_monitor* : " + taskID + " => " + write_quality, "slack")
        else:
            # observation was not found in ALTA
            atdb.atdb_interface.do_PUT(key='observations:quality', id=None, taskid=taskID, value="not_in_alta")
            atdb.report("*ingest_monitor* : ERROR: " + taskID + " was not found in ALTA", "slack")



def do_check_and_send_quality_to_alta(atdb):
    """
    # check if observations have been put on 'go_to_good' or 'go_to_bad'.
    # and if found, send a 'good' or 'bad' quality to ALTA.
    # This is used in conjunction with the 'mark-period-as' functionality of the backend, with which
    # (super)users can mark a range of observations as good/bad for replication/removal from ALTA.

    # note: this function is placed in the 'service_ingest_monitoring' service because this is the
    # only service that communicates with ALTA.
    # This way no extra services is needed, so no supervisor config needs to be changed, so the
    # change to the system is as small as possible.
    # it would be better to rename the service to something like 'service_alta_gateway',
    # but that would also mean a change to supervisor config. Keeping it as simple as possible.

    :param atdb:
    :return:
    """

    handle_quality(atdb,"data_is_good", "good")
    handle_quality(atdb,"data_is_bad", "bad")

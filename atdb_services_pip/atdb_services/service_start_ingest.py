"""
    File name: service_start_ingest.py
    Author: Roy de Goei, Nico Vermaas - Astron
    Date created: 2018-11-23
    Description: This service checks for valid Observations, gathers its valid Dataproducts and starts the
                Ingest to ALTA. At first, the status is set to ingesting (otherwise multiple starts of
                the ingest of the taskid will happen). 
"""

import os
import datetime
import time

try:
    import atdb_ingest
    #import atdb_services.atdb_ingest as atdb_ingest
    skip_ingest = False
except Exception as e:
    skip_ingest_error_message = "WARNING: No atdb_ingest loaded due to import error ->" + str(e) + \
                                "\nContinuing without atdb_ingest"
    skip_ingest = True

INGEST_FAILED_STATUS = "ingest error"
TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

STATUS_START     = 'complete,valid'  # the statusses that trigger this service.
STATUS_END       = 'ingesting'       # this service will leave the observation and dataproducts in this state
STATUS_INCOMPLETE= 'incomplete'
STATUS_VALID_DPS = 'valid'

STATUS_VALID = 'valid'
STATUS_VALID_PRIORITY = 'valid_priority'

# Files the the next extensions are considered as inspection plots file and will be ingested as separate
# irods-collection <taskID>_INSP
INSPECTION_PLOTS_FILES_EXT = ['.png', '.jpg', '.pdf', '.ps']
INSPECTION_PLOTS_COLL_ADDITION = "_INSP"

def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('execution time: %r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000))
        return result
    return timed

def get_size_local(atdb, filepath):
    """
    Get the size of a file (FITS) or directory (MS)
    dataproduct is locally searched through the filesystem
    :param atdb: instance of the atdb_interface
    :param filepath:
    :return: full path to search for (file or dir)
    """
    # get the size of the file (FITS) or directory (MS)
    if os.path.isfile(filepath):
        size = os.path.getsize(filepath)
    else:
        size = atdb.get_dir_size(filepath)
    return size


def get_size_remote(atdb, node, filepath):
    """
    Get the size of a file (FITS)
    dataproduct is remotely searched with a ssh command
    :param atdb: instance of the atdb_interface
    :param filepath: full path to search for (file or dir)
    """
    size = atdb.get_filesize_remote(node,filepath)
    return size


def get_next_ingest(atdb, search_status):
    next_taskID, next_minutes_left = atdb.atdb_interface.do_GET_NextObservation(search_status, atdb.obs_mode_filter, atdb.host_filter)
    return next_taskID

# --------------------------------------------------------------------------------------------------------
def do_start_ingest(atdb, status_filter):

    """
    Main function for starting the atdb_ingest service
    :param atdb:
    :return:
    """
    list_of_irods_collections = []

    #atdb.report('*start_ingest* checks for status_filter ' + str(status_filter))

    if status_filter == None:
        # first check for priority ingests
        taskID = get_next_ingest(atdb, STATUS_VALID_PRIORITY)

        # then check for regular ingests
        if taskID==None:
            taskID = get_next_ingest(atdb, STATUS_VALID)
    else:
        # only check for that status with which this service was started.
        # for example only 'valid' when the service was started with 'status_filter valid '
        # (there will the probably be another service started with 'status_filter valid_priority'
        taskID = get_next_ingest(atdb, status_filter)

    # if a next ingest is found, then execute it
    if taskID!=None:

        irods_collection_name = atdb.atdb_interface.do_GET(key='observations:irods_collection', id=None,
                                                           taskid=taskID)
        atdb.report('*start_ingest* the irods collections of the observation is ' + irods_collection_name)

        ids = atdb.atdb_interface.do_GET_LIST(key='dataproducts:id', query='taskID=' + taskID + '&my_status='+STATUS_VALID_DPS)

        # check if there is anything to ingest at all, if not set the observation status to 'incomplete'
        if len(ids) == 0:
            atdb.atdb_interface.do_PUT(key='observations:new_status', id=None, taskid=taskID, value=STATUS_INCOMPLETE)
            atdb.report("ERROR by *start_ingest* : " + taskID + " " + STATUS_INCOMPLETE,"print,slack")
        else:
            atdb.atdb_interface.do_PUT(key='observations:new_status', id=None, taskid=taskID, value=STATUS_END)
            atdb.report("*start_ingest* service : " + taskID + " " + STATUS_END,"slack")

            # Check if a dataproduct is an inspection plot, in that case change the collection name
            # (just add _INSP which is the ALTA name convention for inspection plots collection)
            for id in ids:
                filename = atdb.atdb_interface.do_GET(key='dataproducts:filename', id=id, taskid=None)
                irods_collection = atdb.atdb_interface.do_GET(key='dataproducts:irods_collection', id=id, taskid=None)
                is_plot = False
                if irods_collection == irods_collection_name:
                    for insp_ext in INSPECTION_PLOTS_FILES_EXT:
                        if insp_ext.lower() in filename:
                            is_plot = True

                # File is an inspection plot so modify collection name
                if is_plot:
                    irods_collection += INSPECTION_PLOTS_COLL_ADDITION
                    atdb.atdb_interface.do_PUT(key='dataproducts:irods_collection', id=id, taskid=None,
                                               value=irods_collection)
                    atdb.report('*start_ingest* inspection plot found, change collection for dataproduct with id='
                                + str(id) + ' to ' + irods_collection)

                list_of_irods_collections.append(irods_collection)

    # All dataproducts are not set with the correct irods_collection
    # remove duplicates from list and ingest the irods_collection list
    list_of_irods_collections = sorted(set(list_of_irods_collections))

    for irods_collection in list_of_irods_collections:
        atdb.report('*start_ingest* ingest to irods collections ' + irods_collection)

        ids = atdb.atdb_interface.do_GET_LIST(key='dataproducts:id', query='irods_collection=' + irods_collection)
        atdb.report('*start_ingest* found the following ' + STATUS_VALID_DPS + ' dataproducts to ingest : ' + str(ids))

        # If a dataproduct has its 'node' value set then this boolean will go to true and
        # a 'remote_dataproducts_to_ingest.txt' file is generated by the atdb_ingest.
        files_are_remote = False

        list_of_valid_files = []
        total_size = 0

        # Gather observation information
        # For ARTS SC1 only we need to ingest the parset file as well. Do not add as dataproducts
        # but just add to list of valid files
        observing_mode = atdb.atdb_interface.do_GET(key='observations:observing_mode', id=None, taskid=taskID)
        if 'ARTS_SC1' in observing_mode.upper() and INSPECTION_PLOTS_COLL_ADDITION not in irods_collection:
            try:
                parset_template = atdb.atdb_interface.do_GET(key='observations:parset_location', id=None, taskid=taskID)
                parset_dir = os.path.split(parset_template)[0]
                parset_full_name = "%s/%s.parset" % (parset_dir, taskID)
                list_of_valid_files.append(parset_full_name)
                total_size += os.path.getsize(parset_full_name)
            # No error if parset file can not be ingested?
            except Exception as err:
                atdb.report("WARNING by *start_ingest* : " + taskID + " can not add parset file to ingest: reason = " + str(err),
                            "print,slack")

        for id in ids:
            # Gather information for the alta_ingest
            # data_location = atdb.atdb_interface.do_GET(key='dataproducts:data_location', id=id, taskid=None)

            data_dir = atdb.atdb_interface.do_GET(key='dataproducts:data_location', id=id, taskid=None)
            # split off the host from the location.
            # host should now always be onboard...
            try:
                _, data_location = data_dir.split(':')
            except:
                # ... just in case the host is not onboard
                # atdb.report("WARNING by *start_ingest* : " + taskID + " no hostname in data_location", "print,slack")
                data_location = data_dir

            filename = atdb.atdb_interface.do_GET(key='dataproducts:filename', id=id, taskid=None)
            filepath = os.path.join(data_location, filename)

            # If a cluster or remote machine is used (like for ARTS SC4) then the 'node' field has the value of
            # a remote machine. In that case the dataproducts on that remote machine are searched.
            # Otherwise the dataproducts are searched on the local machine.
            node = atdb.atdb_interface.do_GET(key='dataproducts:node', id=id, taskid=None)
            size = int(atdb.atdb_interface.do_GET(key='dataproducts:size', id=id, taskid=None))

            # atdb returns a string when a dataproducts is added (like an inspection plot) which is not remote !!
            # so also check for string not None
            if node is not None:
                if node != "None":
                    # dataproducts are local
                    filepath = node+':'+filepath
                    files_are_remote=True

            list_of_valid_files.append(filepath)
            total_size += size

            atdb.report('adding dataproduct ' + filepath + ' for ingest')
            atdb.atdb_interface.do_PUT(key='dataproducts:new_status', id=id, taskid=None, value=STATUS_END)

        # Ingest all valid dataproducts of a (valid) taskid which are located in data_dir
        if not atdb.testmode and not skip_ingest:
            # determine alta irods host from the alta_host parameter, eg
            #  https://alta-acc.astron.nl/altapi --> alta-acc-icat.astron.nl
            #  https://alta.astron.nl/altapi --> alta-icat.astron.nl
            #  http://alta-sys.astron.nl/altapi --> alta-sys-icat.astron.nl
            irods_host = atdb.alta_host.split("://", 1)[1]
            irods_host = irods_host.replace("/altapi", "").replace(".astron","-icat.astron")

            atdb.report('ATDB service start ingest-> To be ingested is ' + str(list_of_valid_files) +
                        ' with total size of ' + str(total_size) + ' bytes to ' + str(irods_host))
            timestamp = datetime.datetime.now().strftime(TIME_FORMAT)
            atdb.report('ATDB service start ingest (taskID=' + str(taskID) + ') -> Start the ALTA ingest '
                        + 'at ' + timestamp + ' ....(wait a moment) \n')

            # Synchronous call...this can take a while.....several hours....
            # retrieve 'quality' and 'max_lifetime_on_disk' from ATDB and feed it to alta_ingest
            locality_policy  = atdb.atdb_interface.do_GET(key='observations:locality_policy', id=None, taskid=taskID)
            max_lifetime_on_disk = atdb.atdb_interface.do_GET(key='observations:max_lifetime_on_disk', id=None, taskid=taskID)

            ret_code, ret_string = atdb_ingest.execute_alta_ingest(taskID, list_of_valid_files, total_size,
                                                               irods_collection, irods_host, locality_policy, max_lifetime_on_disk,
                                                                   files_are_remote)
            if ret_code != 0:
                res_str = "FAILED"
            else:
                res_str = "OK"
            atdb.report('ATDB service start ingest (taskID=' + str(taskID) + ') -> Result of the ALTA ingest is '
                        + res_str + ' with output: ---------->\n'
                        + str(ret_string) + '\n<----------EOF ALTA ingest output ')

            # If ingest failed set the status of the observation and all dataproducts to failed
            if ret_code != 0:
                atdb.atdb_interface.do_PUT(key='observations:new_status', id=None, taskid=taskID,
                                       value=INGEST_FAILED_STATUS)
                atdb.report("ERROR by *start_ingest* : " + taskID + " " + INGEST_FAILED_STATUS,"print,slack")
                for id in ids:
                    atdb.atdb_interface.do_PUT(key='dataproducts:new_status', id=id, taskid=None,
                                               value=INGEST_FAILED_STATUS)
                # We have to leave the for irodscollections loop if the first one failed
                break

        # testmode or skip ingest (same behaviour) just do a fake ingest
        else:
            if skip_ingest:
                atdb.report(skip_ingest_error_message)
            atdb.atdb_interface.do_PUT(key='observations:new_status', id=None, taskid=taskID,
                                       value='ingesting')
            atdb.report("*start_ingest* : " + taskID + " ingesting (fake) ", "slack")
            atdb.report("*start_ingest* :simulate blocking ingest, sleep for 30 seconds)","print,slack")
            time.sleep(int(30))
            # TODO: move the invalid dataproducts away? rename them so that the ingest doesn't recognize them?

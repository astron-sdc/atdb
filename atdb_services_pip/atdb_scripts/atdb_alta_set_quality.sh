#!/bin/sh

# example
# curl -X GET "http://dop457.astron.nl/atdb/mark-period-as?taskid_from=191002130&taskid_to=191002149&quality=data_is_good&observing_mode=imaging"

curl -X GET "http://atdb.astron.nl/atdb/mark-period-as?taskid_from=190403001&taskid_to=190403004&quality=data_is_good&observing_mode=arts_sc4_survey"


# Next command will nocurl -X GET "http://atdb.astron.nl/atdb/mark-period-as?taskid_from=191031202&taskid_to=191031203&quality=data_is_good&observing_mode=imaging"
t close the window, can be handy if something goes wrong
exec $SHELL


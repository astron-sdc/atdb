#!/bin/bash
# Modified from Yogesh Maan's script to connect it to ATDB. 
# Added some extra input parameters and a call to ATDB to put the Observation on 'completing'.
# -- Nico Vermaas, 14 dec 2018

# Modified from Leon Oostrum's script to combine profiles in both time and frequency on ARTS-0,
# A few bug fixes; correct for duplicate beamlets (frequency channels) and make a quick-look plot. -- Yogesh Maan
#
# Exit codes:
# 0: all is fine
# 1: a folder doesn't exist
# 2: mandatory argument(s) missing

# Usage of script
function usage {
echo " Usage: 
 ================================================
 $(basename $0)  <Options>

 
 Options: 
   -taskid  <taskid> : taskid of observation
   -outname_atdb     : output filename according to the specifications in ATDB
   -date    <date>   : date of observation in
                       yyyymmdd format

   -folder  <folder> : subfolder of /data/*/Timing
                       /yyyymmdd which contains
                       the folded profiles

   -sband   <sband>  : Start band number
                      (**Default: 1)

   -eband   <eband>  : End band number
                      (**Default: 16)
 ================================================"
exit 2
}

# Ready to spawn job
function clearToSpawn
{
    local jobcount="$(jobs -r | wc -l)"
    if [ $jobcount -lt $njobs ] ; then
        return 0;
    fi
    return 1;
}


# Call usage if too few arguments are supplied
[[ $# -lt 2 ]] && usage

# parse arguments
while [[ $# -gt 1 ]]; do
    key="$1"

    case $key in
        -sband)
        sband="$2"
        shift
        ;;
        -eband)
        eband="$2"
        shift
        ;;
        -date)
        date="$2"
        shift
        ;;
        -folder)
        folder="$2"
        shift
        ;;
        -taskid)
        taskid="$2"
        shift
        ;;		
        -outname_atdb)
        outname_atdb="$2"
        shift
        ;;			
        -o)
        outname="$2"
        shift
        ;;
        -njobs)
        njobs="$2"
        shift
        ;;
        *)
        usage
        ;;
    esac
    shift
done

while [[ $# -gt 0 ]]; do
    key="$1"

    case $key in
        -full)
        fullres="yes"
        shift
        ;;
    esac
    shift
done

# set defaults for arguments and check whether required arguments are present
sband=${sband:-01}
eband=${eband:-16}
#outname=${outname:-all.ar}
njobs=${njobs:-16}
# Make sure sband starts with 0 if less than 10. eband doesn't matter as we're using seq -w
if [[ $sband -lt 10 ]] && [[ ! ${sband:0:1} -eq 0 ]]; then
    sband=0$sband
fi
# Complain if required arguments are not specified
if [[ "$date" == "" ]]; then
    echo "-date argument is required"
    exit 2
fi
if [[ "$folder" == "" ]]; then
    echo "-folder argument is required"
    exit 2
fi

outname=${folder}.merged.ar  # default output file-name now contains all the information about input filenames
if [[ "$outname_atdb" != "" ]]; then
    outname=${outname_atdb}
fi

# Remove existing band*.ar files
if test -n "$(find . -maxdepth 1 -name 'band??.ar' -print -quit)"; then
    #read -rep 'Some band??.ar files already exist, remove [Y/n]?' ans
    ans="Yes"
    case $ans in 
        [Nn] ) exit 1;;
        *    ) rm -f band??.ar;;
    esac
fi

# Combine in time, Delete the duplicate beamlets and correct the frequencies of all the channels
for band in $(seq -w $sband $eband); do
    fullpath=/data/$band/Timing/$date/$folder
    if [[ ! -d "$fullpath" ]]; then
        echo "Folder $fullpath doesn't exist"
        exit 1
    fi

    # Wait if $njobs jobs are already running
    while ! clearToSpawn ; do
      sleep 0.5
    done

    echo "Combining profiles for band $band"
#    (nice psradd -P $fullpath/*.ar -o band${band}.ar; psredit -m -c bw=18.75 band${band}.ar) &
    fnow=$(echo "1250.0 + ($band - 1) * 12.5" | bc -l)
    for i in `seq 0 15`; do atemp=`echo ${i}*0.781250+${fnow} | bc` ; fstr="${fstr} -c int:freq[$i]=${atemp}" ; done
    cmd="psredit -m ${fstr} band${band}.ar"
    (nice psradd $fullpath/*.ar -o band${band}.ar && /home/arts/scripts/del_chans_subband.psh band${band}.ar && eval ${cmd}) &
done
wait



# Combine in frequency, if multiple bands are used. Otherwise we are done
if [[ $((10#$eband)) -gt $((10#$sband)) ]]; then
    echo "Combining subbands:"
    # if there are too many subintegrations, first do partial scrunch in time
    nsubint=`psrstat -c nsubint -qQ band01.ar`
    if [[ $nsubint -gt 2500 ]]; then
      echo "    Too many subintegrations, first partial-schrunching in time..."
      nice pam -m -t 6 band*.ar
      echo "    Done time-scrunching !"
    fi
    # Using an asterisk here is fine, it won't crash on non-existing files
    nice psradd -R band*.ar -o ${outname}
fi

echo "Done combining !"

## also do basic scrunching and prepare a plot
##nice pam -e .ar.tscr -T ${outname} ; pam -e .ar.fpscr -Fp ${outname} ; psrplot -jDT -p F -D ${outname}0.ps/cps ${outname}.tscr -l pol=0-3


 echo "  "
 
 #echo 'RFI-excision using "paz -rLd"...'
 #nice paz -rLd -m ${outname}.fdel 
 # also do basic scrunching and make plots
 echo "Preparing scrunched archives..."
 nice pam -e .ar.tscr -T ${outname} ; pam -e .ar.fscr -F ${outname}
 echo "Making final plots:"
 echo "    ${outname}f.ps"
 echo "    ${outname}t.ps"
 nice psrplot -jDT -p F -D ${outname}f.ps/cps ${outname}.tscr -l pol=0-3 -c set=pub ; psrplot -jDF -p Y -D ${outname}t.ps/cps ${outname}.fscr -l pol=0-3 -c set=pub 

# set the status of this observation to 'completing' in ATDB.
cd ~/atdb_client
source env2/bin/activate
atdb_interface -o PUT --key observations:new_status --value completing --taskid ${taskid} --host prod

echo "*combine_obs_sc1_for_atdb.sh* created: $outname_atdb"

exit 0


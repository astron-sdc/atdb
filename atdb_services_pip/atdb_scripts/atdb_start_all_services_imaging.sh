#!/bin/sh
# script to start the atdb services
# after pip install, start as production: nohup atdb_start_all_services_imaging.sh

# NV: 6dec2018:
# NOTE! The executor currently handles both 'imaging' and 'arts' observations because of parset_generator import errors on arts0
atdb_service -o executor --obs_mode_filter imaging --interval 10 --atdb_host prod -v &
atdb_service -o executor --obs_mode_filter arts --interval 10 --atdb_host prod -v &

# data_monitor looks for 'completed' observations and its 'defined' dataproducts and puts them on 'completed'
atdb_service -o data_monitor --obs_mode_filter imaging --interval 30 --atdb_host prod -v &

# start_ingest looks for 'valid' tasks and then starts the ingest to ALTA.
atdb_service -o start_ingest --obs_mode_filter imaging --interval 30 --atdb_host prod --alta_host prod -v &

# ingest_monitor looks if 'ingesting' tasks and dataproducts have arrived in ALTA, and when found sets their status in ATDB to 'archived'
atdb_service -o ingest_monitor --obs_mode_filter imaging --interval 30 --atdb_host prod --alta_host prod --user atdb --password V5Q3ZPnxm3uj -v &

# looks for 'archived' observations and deletes its 'archived' dataproducts
atdb_service -o cleanup --obs_mode_filter imaging --interval 60 --atdb_host prod -v &

#!/bin/sh
# ATDB script to start the 'background services' and connects to test backend (VM on 192.168.22.22)
# after pip install, start as test: nohup atdb_start_all_services_test.sh &

# executor looks for scheduled tasks and runs them at starttime
# executor looks for running tasks and stops them at endtime.
atdb_service -o executor --obs_mode_filter imaging --interval 10 --atdb_host test --testmode -v -v2 &

# data_monitor looks for 'completed' observations and its 'defined' dataproducts and puts them on 'completed'
atdb_service -o data_monitor --obs_mode_filter imaging --interval 30 --atdb_host test -v &

# start_ingest looks for 'valid' tasks and then starts the ingest to ALTA.
atdb_service -o start_ingest --obs_mode_filter imaging --interval 30 --atdb_host test -v --testmode &

# ingest_monitor looks if 'ingesting' tasks and dataproducts have arrived in ALTA, and when found sets their status in ATDB to 'archived'
atdb_service -o ingest_monitor --obs_mode_filter imaging --testmode --interval 30 --atdb_host test --alta_host acc --user atdb --password V5Q3ZPnxm3uj -v &

# looks for 'archived' observations and deletes its 'archived' dataproducts
atdb_service -o cleanup --obs_mode_filter imaging --interval 60 --atdb_host test -v &
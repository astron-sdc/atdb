from __future__ import print_function

import sys
import json
import argparse
import pkg_resources
import os
import shutil

# Determine the transfer rate for all archived observation and do a pretty print
import time
from atdb_interface.atdb_interface import ATDB
atdb_interface = ATDB("http://atdb.astron.nl/atdb")

#atdb_interface.do_PUT(key='observations:control_parameters', taskid=taskid_str, value=progress)

LAST_UPDATE = "28 May 2020"



def remove_from_atdb(taskid, cb_str, real_mode=False):
    """
    Remove unnecessary dataproducts from ATDB.
    Remove CB<id> from taskID
    :param taskid: string
    :param cb_str: Compound Beams ID String
    :param real_mode: Then remove from datawriter and delete/update ATDB
    """
    mode_str = ""
    if not real_mode:
        mode_str = "NON-REAL MODE (do nothing)"

    # find the observation ID
    id = atdb_interface.do_GET_ID(key='observations:taskID', value=taskid)
    print("\nTaskID %s (observation ID %d)" % (taskid, id))

    # get the list of ids of all dataproducts
    # Add file names to parset, retrieve all dataproducts for taskid
    ids = atdb_interface.do_GET_LIST(key='dataproducts:id', query='taskID=' + taskid)
    remove_ids = []
    remove_size = 0
    for id in ids:
        filename = atdb_interface.do_GET(key='dataproducts:filename', id=id, taskid=None)
        if cb_str in filename and taskid in filename:
            size = int(atdb_interface.do_GET(key='dataproducts:size', id=id, taskid=None))
            print("Add dataproduct '%s' (id=%d) with size %d to remove" % (filename, id, size))
            remove_ids.append(id)
            remove_size += size

    print("The following dataproducts ids will be removed %s" % str(remove_ids) )

    # Remove DPS from ATDB
    for id in remove_ids:
        data_location = atdb_interface.do_GET(key='dataproducts:data_location', id=id, taskid=None)
        filename = atdb_interface.do_GET(key='dataproducts:filename', id=id, taskid=None)
        print("REMOVE dataproduct '%s' (id=%d) from ATDB %s" % (filename, id, mode_str))
        if real_mode:
           atdb_interface.do_DELETE(resource='dataproducts', id=id)

    # Update size of the observation
    org_size = int(atdb_interface.do_GET(key='observations:size', id=None, taskid=taskid))
    size = org_size - remove_size
    print("Update size of TaskID %s from %d to %d %s" % (str(taskid), org_size, size, mode_str))
    if real_mode:
        atdb_interface.do_PUT(key='observations:size', id=None, taskid=taskid, value=size)


def process_single_taskid(args):
    """
    Just a wrapper tp process single observation
    :param args:
    :return:
    """

    taskid = args.taskid
    cb_str = "CB%02d" % int(args.cb)
    print("The observations of taskID '%s' will be changed in ATDB. " % taskid)
    print("The dataproducts of Compound Beam '%s' will be removed" % cb_str)
    if args.real_mode:
        print("WATCH OUT IT IS REAL MODE!!!")
    else:
        print("Dry run, so do no real actions")
    if args.answer_yes:
        answer = "Y"
    else:
        answer = raw_input("\nAre you sure you want to continue (Y/N)? \n")
    if answer.upper() == "Y":
        remove_from_atdb(taskid, cb_str, args.real_mode)
        print("Done")
    else:
        print("You did not confirm so exit with doing nothing")



# ------------------------------------------------------------------------------#
#                                Main                                           #
# ------------------------------------------------------------------------------#
def main():
    """
    The main module.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--taskid', nargs='?', default='', type=str,
                        action="store",
                        help='Specify the taskID of the ARTSC4 observation')
    parser.add_argument('--cb', nargs='?', default='', type=str,
                        action="store",
                        help='Specify the CB id (00-39)')
    parser.add_argument("--real_mode", default=False,
                        help="If enabled the ATDB will be adjusted.",
                        action="store_true")
    parser.add_argument("--version", default=False,
                        help="Show current version of this program.",
                        action="store_true")
    parser.add_argument("--answer_yes", "-Y", default=False,
                        help="Force Yes do not ask for confirmation.",
                        action="store_true")
    parser.add_argument("--batch_mode", action='store', dest='batch_params', default=None, nargs=3,
                        help="Execute in batch modes from start to end taskid and with possible multiple cb.")
    # Example batch mode (no confirmation per taskid):
    # python alta_remove_CB_from_observation.py --batch_mode 200512001 200515099 "6, 10" --real_mode
    args = parser.parse_args()

    if args.version:
        print("--- alta_remove_CB_from_observation.py (last updated " + LAST_UPDATE + ") ---")
        return

    try:
        if args.batch_params:
            first_taskid = args.batch_params[0]
            last_taskid = args.batch_params[1]
            list_cb = args.batch_params[2].split(",")
            # Now get list of all taskid between first and last (should be SC4) with state valid and mode arts_sc4
            query = "taskID__gte=%s&taskID__lte=%s&my_status__icontains=valid&observing_mode__icontains=arts_sc4" % \
                    (first_taskid, last_taskid)
            lst_taskids = atdb_interface.do_GET_LIST(key='observations:taskID', query=query)
            print("Found '%d' taskIDs to process" % len(lst_taskids))
            print(lst_taskids)
            if args.real_mode:
                print("WATCH OUT IT IS REAL MODE!!!")
            else:
                print("Dry run, so do no real actions")
            answer = raw_input("\nAre you sure you want to continue (Y/N)? \n")
            if answer.upper() != "Y":
                print("OK ...we quit")
            else:
                for taskid in lst_taskids:
                    for cb in list_cb:
                        cb_str = "CB%02d" % int(cb)
                        print("The observations of taskID '%s' will be changed in ATDB. "
                              "The dataproducts of Compound Beam '%s' will be removed" % (taskid, cb_str))
                        remove_from_atdb(taskid, cb_str, args.real_mode)
                print("Done")
        else:
            process_single_taskid(args)

    except Exception as exp:
        print("FATAL ERROR " + str(exp))

    sys.exit(0)


if __name__ == "__main__":
    main()

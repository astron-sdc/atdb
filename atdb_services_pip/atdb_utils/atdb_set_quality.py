""""
Set a range of observations to bad.
If the the locality_policy is wrong (locality_policy) then set it to archive, otherwise it will not be deleted from ALTA

"""

from __future__ import print_function


import argparse
import sys
import traceback

from atdb_interface.atdb_interface import ATDB
atdb_interface = ATDB("http://atdb.astron.nl/atdb")

# The request header
ATDB_HEADER = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'authorization': "Basic YWRtaW46YWRtaW4="
}


def update_observations_with_wrong_locality_policy_value():
    """
    Note ATDB only get max 100 thats OK
    http://atdb.astron.nl/atdb/observations/?locality_policy=locality_policy&quality=bad
    """
    ids = atdb_interface.do_GET_LIST(key='observations:id', query='&locality_policy=locality_policy&quality=bad')
    print("The number of observations which are marked as bad BUT have wrong locality_policy is %d " % len(ids))
    for id in ids:
        print("Update %d" % id)
        atdb_interface.do_PUT(key='observations:locality_policy', id=id, taskid=None, value="archive")


def set_observation_quality(taskid_str, quality_str):
    """
    Set for the observation given taskid, the quality
    and if the locality_policy is 'wrong' set it to the default 'archive'
    :param taskid_str: First taskID (string)
    :param quality_str: Quality attribute (string)

    """
    print("The observation with taskID '%s' will be changed to '%s' quality" % (taskid_str, quality_str))
    try:
        # atdb_interface.do_PUT(key='observations:quality', id=None, taskid=taskid_str, value="bad")
        # simply PUT does not work, because the ATDB 'setquality' operation must be executed also.
        # The 'atdb_interface.do_setquality' function can do that now
        atdb_interface.do_setquality(key='observations:quality', id=None, taskid=taskid_str, value=quality_str)

        locality_policy = atdb_interface.do_GET(key='observations:locality_policy', id=None, taskid=taskid_str)
        if locality_policy == "locality_policy":
            print("Update locality_policy to archive")
            atdb_interface.do_PUT(key='observations:locality_policy', id=None, taskid=taskid_str, value="archive")
    except Exception as exp:
        print("ERROR Can not set quality %s of %s" % (quality_str, taskid_str))
        print("exception" + str(exp))
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print(repr(traceback.format_exception(exc_type, exc_value, exc_traceback)))


def get_all_taskids_inrange(first_taskid, last_taskid):
    """
    Get all taskIDS between the first and last given taskID
    Use 'page' method to get more than 100 results if needed
    :param first_taskid: First taskID (int)
    :param last_taskid: Last taskID (int)
    :return: lst_taskids: List of taskIDS (string)
    """
    query = '&taskID__gte=%d&taskID__lte=%d' % (first_taskid, last_taskid)
    # gather the list of taskIDs to check
    count = 0
    page = 1
    lst_taskids = []
    while page > 0:
        taskIDs, total_count, next_page = atdb_interface.do_GET_LIST_page(key='observations:taskID',
                                                                          query=query, page=page)
        lst_taskids += taskIDs
        page = int(next_page)
    lst_taskids.sort()
    return lst_taskids


def get_total_size(quality_str):
    """
    Get the total size of the given quality attribute
    Use 'page' method to get more than 100 results if needed
    :param quality_str: Quality attribute (string)
    :return: total_size: Total size in bytes
    """
    total_size = 0
    query = 'quality=%s' % quality_str
    page = 1
    while page > 0:
        size, total_count, next_page = atdb_interface.do_GET_LIST_page(key='observations:size',
                                                                       query=query, page=page)
        total_size += size
        page = int(next_page)
    return total_size


# ------------------------------------------------------------------------------#
#                                Main                                           #
# ------------------------------------------------------------------------------#
def main():
    """
    The main module.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--first_tid', type=int,
                        help='Specify the first taskID to set to quality',
                        action="store")
    parser.add_argument('--last_tid', type=int,
                        help='Specify the last taskID to set to quality',
                        action="store")
    parser.add_argument('--taskid', type=int, nargs="+",
                        help='Specify a list of taskID (spaces) to set to quality',
                        action="store")
    parser.add_argument('--quality', type=str, default="data-is-bad",
                        help='Specify the quality attribute; data-is-bad (default), data-is-good or unknown',
                        action="store")
    parser.add_argument('--update_observations',
                        help='Update all wrong locality_policy when quality is set',
                        action="store_true")
    parser.add_argument('--show_total_size',
                        help='Show the total size of items of the the given quality',
                        action="store_true")

    args = parser.parse_args()
    lst_taskids = []

    try:
        if args.update_observations:
            update_observations_with_wrong_locality_policy_value()

        if args.first_tid and args.first_tid:
            lst_taskids = get_all_taskids_inrange(int(args.first_tid), int(args.last_tid))
            print("Number of taskIDs found to set to quality is %d" % len(lst_taskids))

        if args.taskid:
            lst_taskids = args.taskid

        for taskid in lst_taskids:
            set_observation_quality(str(taskid), str(args.quality))

        if args.show_total_size:
            total_size = get_total_size(str(args.quality))
            print("Total size is %f [GB]" % (total_size/1000000000))

    except Exception as exp:
        print("FATAL ERROR " + str(exp))

if __name__ == "__main__":
    main()
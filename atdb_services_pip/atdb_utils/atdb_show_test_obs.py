from __future__ import print_function

import requests
import json
import argparse
import datetime
import time
import sys

from atdb_interface.atdb_interface import ATDB
atdb_interface = ATDB("http://atdb.astron.nl/atdb")

# The request header
ATDB_HEADER = {
    'content-type': "application/json",
    'cache-control': "no-cache",
    'authorization': "Basic YWRtaW46YWRtaW4="
}

def show_test_observations():
    """
    Note ATDB only get max 100
    """
    lst_taskid = []
    ids = my_GET_LIST()
    # ids = atdb_interface.do_GET_LIST(key='observations:id', query='&my_status__in=archived,removed&observing_mode__in=imaging')
    print("Max.number of 'imaging' observations NOT science retrieved from ATDB with status 'archived' or 'removed' (ingested to ALTA) is %d " % len(ids))
    for id in ids:
        duration = atdb_interface.do_GET(key='observations:duration', id=id, taskid=None)
        #print(duration)
        if duration <= 60:
            # Get TaskID
            taskid = atdb_interface.do_GET(key='observations:taskID', id=id, taskid=None)
            lst_taskid.append(taskid)

    # remove duplicates
    lst_taskid.list(set(lst_taskid))
    lst_taskid.sort(reverse=True) # Latest TaskIDs first
    print("Number of TaskIDs found with duration of 60 seconds is %d" % len(lst_taskid))
    for taskid in lst_taskid:
        size = atdb_interface.do_GET(key='observations:size', id=None, taskid=taskid)
        tasktype = atdb_interface.do_GET(key='observations:task_type', id=None, taskid=taskid)
        science_obs = atdb_interface.do_GET(key='observations:science_observation', id=None, taskid=taskid)
        size_inGB = int(size) / 1000000000
        print("taskID = %s  size %d [GB]  task_type =%s  science_observation = %s" %
              (taskid, size_inGB, tasktype, science_obs))

#  python atdb_iterface.py -o GET_LIST --key observations:taskID --query status=valid
def my_GET_LIST():
    """
    Do a http GET request to the ATDB backend to find the value of one field of an object
    :param key: contains the name of the resource and the name of the field separated by a dot.
    :param id: the database id of the object.
    :param taskid (optional): when the taskid (of an activity) is known it can be used instead of id.
    """
    next_url = "http://atdb.astron.nl/atdb/observations?&my_status__in=archived,removed&observing_mode__in=imaging&science_observation__equals=false"
    print("First url = %s" % next_url)
    lst_all_id = []
    while next_url is not None:
        lst_id, next_url = get_id(next_url)
        lst_all_id += lst_id
    return lst_all_id

def get_id(url):
    """
    Do a http GET request to the ATDB backend to find the value of one field of an object
    :param key: contains the name of the resource and the name of the field separated by a dot.
    :param id: the database id of the object.
    :param taskid (optional): when the taskid (of an activity) is known it can be used instead of id.
    :return result field and next url
    """
    response = requests.request("GET", url, headers=ATDB_HEADER)
    print("[GET " + response.url + "]")
    print("Response: " + str(response.status_code) + ", " + str(response.reason))
    list_id = []
    next_url = None
    try:
        json_response = json.loads(response.text)
        results = json_response["results"]
        next_url = json_response["next"]
        # results = json.loads(response.text)
        # loop through the list of results and extract the requested field (probably taskID),
        # and add it to the return list.
        for res in results:
            list_id.append(res["id"])
    except Exception as err:
        print("ERROR: " + str(response.status_code) + ", " + str(response.reason) + ', ' + str(response.content))

    print("Length of list %d with content %s " % (len(list_id), str(list_id)))
    print("Next url = %s" % next_url)

    return list_id, next_url




# ------------------------------------------------------------------------------#
#                                Main                                           #
# ------------------------------------------------------------------------------#
def main():
    """
    The main module.
    :return:
    """
    show_test_observations()
    print("Done")
    sys.exit(0)


if __name__ == "__main__":
    main()

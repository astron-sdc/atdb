from __future__ import print_function

import sys
import json
import argparse
import pkg_resources
import os
import shutil

# Determine the transfer rate for all archived observation and do a pretty print
import time
from atdb_interface.atdb_interface import ATDB
atdb_interface = ATDB("http://atdb.astron.nl/atdb")

#atdb_interface.do_PUT(key='observations:control_parameters', taskid=taskid_str, value=progress)

LAST_UPDATE = "01 July 2019"



def remove_from_atdb(first_taskid, last_taskid, real_mode=False):
    """
    Remove unnecessary dataproducts from ATDB from first_taskid until last_taskid
    Assume 40 beams. The name of the observation will determine which beam should stay,
    all others will be removed
    :param first_taskid: integer
    :param last_taskid: integer
    :param real_mode: Then remove from datawrite and delete/update ATDB
    """
    mode_str = ""
    if real_mode==False:
        mode_str = "NON-REAL MODE (do nothing)"

    for taskid in range(first_taskid, last_taskid+1):
        fieldname = atdb_interface.do_GET(key='observations:field_name', id=None, taskid=str(taskid))
        #print("TaskID %d has fieldname is %s" % (taskid, fieldname))
        beamid = int(fieldname.split('_')[1])
        keep_dataproduct = "WSRTA%d_B%03d.MS" % (taskid, beamid)
        #print("ONLY KEEP ONLY %s (remove the rest)" % keep_dataproduct)

        # find the observation ID
        id = atdb_interface.do_GET_ID(key='observations:taskID', value=str(taskid))
        print("\nTaskID %d (observation ID %d)" % (taskid, id))

        # get the list of ids of all dataproducts
        # Add file names to parset, retrieve all dataproducts for taskid
        ids = atdb_interface.do_GET_LIST(key='dataproducts:id', query='taskID=' + str(taskid))
        remove_ids = []
        size = 0
        for id in ids:
            filename = atdb_interface.do_GET(key='dataproducts:filename', id=id, taskid=None)
            if filename == keep_dataproduct:
                size = int(atdb_interface.do_GET(key='dataproducts:size', id=id, taskid=None))
                print("KEEP dataproduct '%s' (id=%d) with size %d" % (filename, id, size))
            else:
                remove_ids.append(id)
        print("The following dataproducts ids will be removed %s" % str(remove_ids) )

        # Remove DPS from ATDB
        for id in remove_ids:
            data_location = atdb_interface.do_GET(key='dataproducts:data_location', id=id, taskid=None)
            filename = atdb_interface.do_GET(key='dataproducts:filename', id=id, taskid=None)
            print("Remove dataproduct '%s' (id=%d) from ATDB %s" % (filename, id, mode_str))
            if real_mode:
               atdb_interface.do_DELETE(resource='dataproducts', id=id)

            # The most dangerous part, remove from datawriter
            filepath = os.path.join(data_location, filename)
            print("Removing from datawriter '%s' %s" % (filepath, mode_str))
            if real_mode and ".MS" in filepath and "WSRTA" in filepath:
                shutil.rmtree(filepath)

        # Update size of the observation which is equal to one dps
        print("Update size of TaskID %s to %d %s" % (str(taskid), size, mode_str))
        if real_mode:
            atdb_interface.do_PUT(key='dataproducts:size', id=id, taskid=None, value=size)


# ------------------------------------------------------------------------------#
#                                Main                                           #
# ------------------------------------------------------------------------------#
def main():
    """
    The main module.
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--taskid', nargs='?', default='', type=str,
                        action="store",
                        help='Specify the first taskID (B000) of the B000 observation. (Assume 40 beams)')
    parser.add_argument("--real_mode", default=False,
                        help="If enabled the ATDB will be adjusted and files removed.",
                        action="store_true")
    parser.add_argument("--version", default=False,
                        help="Show current version of this program.",
                        action="store_true")

    args = parser.parse_args()

    if args.version:
        print("--- alta_adjust_observation_one_beams.py (last updated " + LAST_UPDATE + ") ---")
        return


    try:
        first_taskid = args.taskid
        last_taskid = str(int(first_taskid) + 39)
        print("The observations of taskID %s until %s will be changed in ATDB. " % (first_taskid, last_taskid))
        print("Only 1 beam per observation (depending on name). All other dataproducts will be removed")
        if args.real_mode:
            print("WATCH OUT IT IS REAL MODE!!!")
        else:
            print("Dry run, so do no real actions")
        answer = raw_input("\nAre you sure you want to continue (Y/N)? \n")
        if answer.upper() == "Y":
            remove_from_atdb(int(first_taskid), int(last_taskid), args.real_mode)
            print("Done")
        else:
           print("You did not confirm so exit with doing anything")



    except Exception as exp:
        print("FATAL ERROR " + str(exp))

    sys.exit(0)


if __name__ == "__main__":
    main()

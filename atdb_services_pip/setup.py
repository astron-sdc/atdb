from setuptools import setup, find_packages

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='atdb_services',
      version='1.2.11',
      description='ATDB services',
      url='https://www.astron.nl/wsrt/wiki/doku.php?id=atdb:atdb_services',
      author='Nico Vermaas & Roy de Goei - Astron',
      author_email='vermaas@astron.nl',
      license='BSD',
      install_requires=['requests'],
      packages=find_packages(),
      include_package_data=True,
      entry_points={
            'console_scripts': [
                  'atdb_service=atdb_services.atdb_service:main',
            ],
      },
      scripts=['atdb_scripts/atdb_start_all_services_all.sh',
               'atdb_scripts/atdb_start_all_services_imaging.sh',
               'atdb_scripts/atdb_start_all_services_arts.sh',
               'atdb_scripts/atdb_start_all_services_arts_sc1.sh',
               'atdb_scripts/atdb_start_all_services_arts_sc4.sh',
               'atdb_scripts/atdb_start_all_services_test_all.sh',
               'atdb_scripts/atdb_start_all_services_test_imaging.sh',
               'atdb_scripts/atdb_start_all_services_test_arts.sh',
               'atdb_scripts/atdb_kill_all_services.sh',
               'atdb_scripts/atdb_change_status.sh',
               'atdb_scripts/atdb_delete.sh',
               'atdb_scripts/atdb_scheduler.sh',
               'atdb_scripts/combine_obs_sc1_for_atdb.sh'],
      )
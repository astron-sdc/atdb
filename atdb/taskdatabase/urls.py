from django.urls import path

from . import views

urlpatterns = [
    # --- GUI ---
    # ex: /atdb/
    #path('', views.index, name='index'),
    path('', views.IndexView.as_view(), name='index'),

    # ex: /atdb/task/180223003/
    path('task/<taskID>/', views.DataProductsListView.as_view(), name='dataproducts-list-view'),

    # --- REST API ---
    # ex: /atdb/dataproducts/
    path('dataproducts/', views.DataProductListViewAPI.as_view()),

    # ex: /atdb/dataproducts/5/
    path('dataproducts/<int:pk>/', views.DataProductDetailsViewAPI.as_view(),name='dataproduct-detail-view-api'),

    # ex: /atdb/observations/
    path('observations/', views.ObservationListViewAPI.as_view()),

    # ex: /atdb/observations_unpaginated/
    path('observations_unpaginated/', views.ObservationListUnpaginatedViewAPI.as_view()),

    # ex: /atdb/observations/5/
    path('observations/<int:pk>/', views.ObservationDetailsViewAPI.as_view(),name='observation-detail-view-api'),

    # ex: /atdb/status/
    path('status/', views.StatusListViewAPI.as_view(),name='status-list-view-api'),

    # --- custom requests ---
    # ex: /atdb/get_next_taskid?timestamp=2019-04-05
    path('get_next_taskid',
         views.GetNextTaskIDView.as_view(),
         name='get-next-taskid-view'),

    # ex: /atdb/get_next_observation?my_status=scheduled&observing_mode=imaging
    path('get_next_observation',
         views.GetNextObservationView.as_view(),
         name='get-next-observation-view'),

    # ex: /atdb/post_dataproducts&taskid=190405034
    path('post_dataproducts',
         views.PostDataproductsView.as_view(),
         name='post-dataproducts-view'),

    # --- reports ---
    # get observing times and ingest times
    path('times', views.GetTimesView.as_view(), name='get-times'),
    path('times-drf', views.GetTimesViewDRF.as_view(), name='get-times-drf'),
    path('speeds', views.ReportSpeedsView.as_view(), name='report-speeds'),

    # ex: /atdb/timeused?from=2019-06-01T00:00:00Z&to=2019-06-08T00:00:00Z
    path('time-used', views.ReportTimeUsedView.as_view(), name='time-used'),

    # --- controller resources ---
    # ex: /atdb/mark-period-as?from=2019-06-01T00:00:00Z&to=2019-06-08T00:00:00Z&type=science,system,filler
    path('mark-period-as', views.MarkPeriodAsView.as_view(), name='mark-period'),


    path('observations/<int:pk>/setstatus/<new_status>/<page>',
         views.ObservationSetStatus,
         name='observation-setstatus-view'),
    path('observations/<int:pk>/setstatus_dps/<new_dps_status>/<new_obs_status>/<page>',
         views.ObservationSetStatusDataProducts,
         name='observation-dps-setstatus-view'),
    path('dataproducts/<int:pk>/setstatus/<new_status>',
         views.DataProductSetStatusView,
         name='dataproduct-setstatus-view'),
    # set the quality field to 'good' or 'bad' (and transmit it to ALTA)
    path('observations/<int:pk>/setquality/<quality>/<page>',
         views.ObservationSetQuality,
         name='observation-setquality-view'),
    path('observations/<int:pk>/setdatawriter/<datawriter>/<page>',
         views.SetDatawriter,
         name='observation-setdatawriter-view'),
    path('observations/<int:pk>/skipautoingest/<skip_it>/<page>',
         views.SkipAutoIngest,
         name='observation-skipautoingest-view'),
]
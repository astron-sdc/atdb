from django.db import models
from django.urls import reverse
from django.utils.timezone import datetime
# from .services import algorithms
from django.db.models import Sum
from .services.common import timeit

# constants
datetime_format_string = '%Y-%m-%dT%H:%M:%SZ'

TASK_TYPE_OBSERVATION = 'observation'
TASK_TYPE_DATAPRODUCT = 'dataproduct'

TYPE_VISIBILITY = 'visibility'

# common model functions
def get_sum_from_dataproduct_field(taskID, field):
    """
    sum the values of a field for certain taskID. (used to sum total of dataproduct sizes)
    :param taskID:
    :param field:
    :return:
    """
    query = field + '__sum'
    sum_value = DataProduct.objects.filter(taskID=taskID).aggregate(Sum(field))[query]
    if sum_value == None:
        sum_value = 0.0
    return sum_value

"""
a base class of both Observation and Dataproducts
"""
class TaskObject(models.Model):
    name = models.CharField(max_length=100, default="unknown")
    task_type = models.CharField(max_length=20, default=TASK_TYPE_DATAPRODUCT)

    taskID = models.CharField('runId', db_index=True, max_length=30, blank=True, null=True)
    creationTime = models.DateTimeField(default=datetime.utcnow, blank=True)

    new_status = models.CharField(max_length=50, default="defined", null=True)
    data_location = models.CharField(max_length=255, default="unknown",null=True)
    irods_collection = models.CharField(max_length=255, default="unknown", null=True)

    # my_status is 'platgeslagen', because django-filters can not filter on a related property,
    # and I need services to be able to filter on a status to execute their tasks.
    my_status = models.CharField(db_index=True, max_length=50,default="defined")
    node = models.CharField(max_length=10, null=True)

    locality_policy = models.CharField(max_length=100, default="cold_tape")
    # default 30 days (in minutes)
    max_lifetime_on_disk = models.IntegerField('max_lifetime_on_disk', default=86400)

    def __str__(self):
        return str(self.id)


class Status(models.Model):
    name = models.CharField(max_length=50, default="unknown")
    timestamp = models.DateTimeField(default=datetime.utcnow, blank=True)
    taskObject = models.ForeignKey(TaskObject, related_name='status_history', on_delete=models.CASCADE, null=False)

    @property
    def property_taskID(self):
        return self.taskObject.taskID

    @property
    def property_task_type(self):
        return self.taskObject.task_type

    # the representation of the value in the REST API
    def __str__(self):
        formatedDate = self.timestamp.strftime(datetime_format_string)
        return str(self.name)+' ('+str(formatedDate)+')'


class Observation(TaskObject):
    starttime = models.DateTimeField('start time', null=True)
    endtime = models.DateTimeField('end time', null=True)

    # beamPattern is used to understand how many beams (dataproducts) must be created
    beamPattern = models.CharField(max_length=50, null=True)

    # can be used to distinguish types of observations, like for ARTS.
    process_type = models.CharField(max_length=50, default="observation")
    observing_mode = models.CharField(max_length=50, default="imaging")
    science_mode = models.CharField(max_length=50, default="", null = True)

    # json object containing unmodelled parameters that are used by the 'executor' service
    # to create the parset based on a template and these parameters
    field_name = models.CharField(max_length=50, null=True)
    field_ra = models.FloatField('field_ra', null = True)
    field_ha = models.FloatField('field_ha', null=True)
    field_dec = models.FloatField('field_dec', null = True)
    field_beam = models.IntegerField('field_beam', default=0)
    integration_factor = models.IntegerField('integration_factor', null = True)
    central_frequency = models.FloatField('central_frequency', null = True)

    control_parameters = models.CharField(max_length=255, default="unknown", null=True)
    telescopes = models.CharField(max_length=100, default="all", null=True)
    skip_auto_ingest = models.BooleanField(default=False)
    parset_location = models.CharField(max_length=255,
                                       default="/opt/apertif/share/parsets/parset_start_observation_atdb.template",
                                       null=True)

    delay_center_offset = models.CharField(max_length=50, null=True)

    # ARTS SC1
    par_file_name = models.CharField(max_length=255, default="source.par", null=True)
    number_of_bins = models.IntegerField(null=True)
    start_band = models.IntegerField(null=True)
    end_band = models.IntegerField(null=True)

    # ARTS SC4
    process_triggers = models.BooleanField(default=False)
    beams = models.CharField(max_length=255, default="0..39")

    quality = models.CharField(max_length=30, default="unknown")

    science_observation = models.BooleanField(default=False)
    filler = models.BooleanField(default=False)
    ingest_progress = models.CharField(max_length=40, default="", null=True)

    # several reporting queries use timestamps from the status history
    # it is expensive to look for, so this is some redundancy for performance
    timestamp_starting = models.DateTimeField('timestamp_starting', null=True)
    timestamp_running = models.DateTimeField('timestamp_running', null=True)
    timestamp_completing = models.DateTimeField('timestamp_completing', null=True)
    timestamp_aborted = models.DateTimeField('timestamp_aborted', null=True)
    timestamp_ingesting = models.DateTimeField('timestamp_ingesting', null=True)
    timestamp_archived = models.DateTimeField('timestamp_archived', null=True)
    timestamp_ingest_error = models.DateTimeField('timestamp_ingest_error', null=True)

    # this translates a view-name (from urls.py) back to a url, to avoid hardcoded url's in the html templates
    # bad : <td><a href="/atdb/observations/{{ observation.id }}/" target="_blank">{{ observation.taskID }} </a> </td>
    # good: <td><a href="{{ observation.get_absolute_url }}" target="_blank">{{ observation.taskID }} </a> </td>
    def get_absolute_url(self):
        return reverse('observation-detail-view-api', kwargs={'pk': self.pk})

    @property
    def duration(self):
        try:
            duration = (self.endtime - self.starttime).seconds
        except:
            # to prevent crash for invalid observations that do not have a starttime
            duration = 0
        return duration

    @property
    def size(self):
        # sum the sizes of all dataproducts with this taskID. In Mb
        size = get_sum_from_dataproduct_field(self.taskID,'size')
        return size

    def __str__(self):
        return str(self.taskID)


class DataProduct(TaskObject):
    # properties
    filename = models.CharField(max_length=200, default="unknown")
    description = models.CharField(max_length=255, default="unknown")
    dataproduct_type = models.CharField('type', default=TYPE_VISIBILITY, max_length=50)
    size = models.BigIntegerField(default=0)
    quality = models.CharField(max_length=30, default="unknown")

    # relationships
    parent = models.ForeignKey(Observation, related_name='generated_dataproducts', on_delete=models.CASCADE, null=False)

    # this translates a view-name (from urls.py) back to a url, to avoid hardcoded url's in the html templates
    # bad : <td><a href="/atdb/observations/{{ observation.id }}/" target="_blank">{{ observation.taskID }} </a> </td>
    # good: <td><a href="{{ observation.get_absolute_url }}" target="_blank">{{ observation.taskID }} </a> </td>
    def get_absolute_url(self):
        return reverse('dataproduct-detail-view-api', kwargs={'pk': self.pk})

    def __str__(self):
        return self.filename

# --- Models for atdb_reporting ---------------------------------------

def get_timestamp_status(self, taskID, status):
    """
    get the timestamp of a status for an observation with this taskID
    :param taskID:
    :param status:
    :return:
    """
    # for backward compatibility... since 26 july 2019 these timestamp fields are explicitly defined in the model.
    # but old obervatons do not have them filled in, and there they have to be read from the (slow) status history.

    # first try the new (fast) field.
    timestamp = None
    if status == 'starting':
        timestamp = self.timestamp_starting
    elif status == 'running':
        timestamp = self.timestamp_running
    elif status == 'completing':
        timestamp = self.timestamp_completing
    elif status == 'ingesting':
        timestamp = self.timestamp_ingesting
    elif status == 'archived':
        timestamp = self.timestamp_archived
    elif status == 'aborted':
        timestamp = self.timestamp_aborted
    elif status == 'ingest error':
        timestamp = self.timestamp_ingest_error

    # then try the old (slow) status history mechanism
    if timestamp == None:
        queryset = Status.objects.filter(taskObject__taskID=taskID).filter(
            taskObject__task_type='observation').filter(name__icontains=status).order_by('-timestamp')
        if len(queryset) > 0:
            observation = queryset[0]
            timestamp = observation.timestamp
    return timestamp

# only retrieve a limited number of  fields (for better performance)
class TimesManager(models.Manager):
    def get_queryset(self):
        return super(TimesManager, self).get_queryset().only('taskID','observing_mode','endtime','starttime')


class Times(Observation):
    """
    # this is a proxy model of Observation for reporting.
    # What is this? : https://www.benlopatin.com/using-django-proxy-models/
    """
    objects = TimesManager()

    @property
    def duration(self):
        try:
            duration = (self.endtime - self.starttime).seconds
        except:
            # to prevent crash for invalid observations that do not have a starttime
            duration = None
        return duration

    @property
    def total_size(self):
        # sum the sizes of all dataproducts with this taskID. In Mb
        size = get_sum_from_dataproduct_field(self.taskID, 'size') / 1e6
        return size

    @property
    def write_speed(self):
        speed = None
        if (self.total_size!=None) and (self.duration!=None):
            speed = self.total_size / self.duration
        return speed

    @property
    def timestamp_ingesting_derived(self):
        timestamp = get_timestamp_status(self, self.taskID, 'ingesting')
        return timestamp

    @property
    def timestamp_ingest_error_derived(self):
        timestamp = get_timestamp_status(self, self.taskID, 'ingest error')
        return timestamp

    @property
    def timestamp_archived_derived(self):
        timestamp = get_timestamp_status(self, self.taskID, 'archived')
        return timestamp

    @property
    def ingest_duration(self):

        duration = None
        # calculate the number of seconds between the to timestamps
        if (self.timestamp_ingesting_derived!=None) and (self.timestamp_archived_derived!=None):
            duration = (self.timestamp_archived_derived - self.timestamp_ingesting_derived).total_seconds()

        # in case of an ingest error there is also a duration (albeit small)
        elif (self.timestamp_ingesting_derived!=None) and (self.timestamp_ingest_error_derived!=None):
            duration = (self.timestamp_ingest_error_derived - self.timestamp_ingesting_derived).total_seconds()

        return duration

    @property
    def ingest_speed(self):
        speed = None
        if (self.total_size!=None) and (self.ingest_duration!=None):
            speed = self.total_size / self.ingest_duration
        return speed


    class Meta:
        proxy = True


# only retrieve a limited number of  fields (for better performance)
class TimeUsedManager(models.Manager):
    def get_queryset(self):
        return super(TimeUsedManager, self).get_queryset().only('taskID','observing_mode','endtime','starttime')


class TimeUsed(Observation):
    """
    # this is a proxy model of Observation for reporting.
    # What is this? : https://www.benlopatin.com/using-django-proxy-models/
    """
    objects = TimeUsedManager()

    @property
    def duration(self):
        try:
            duration = (self.endtime - self.starttime).seconds
        except:
            # to prevent crash for invalid observations that do not have a starttime
            duration = None
        return duration

    @property
    def timestamp_running_derived(self):
        timestamp = get_timestamp_status(self, self.taskID, 'running')
        return timestamp

    @property
    def timestamp_aborted_derived(self):
        timestamp = get_timestamp_status(self, self.taskID, 'aborted')
        return timestamp

    @property
    def timestamp_archived_derived(self):
        timestamp = get_timestamp_status(self, self.taskID, 'archived')
        return timestamp

    class Meta:
        proxy = True
"""
    File name: algorithms.py
    Author: Nico Vermaas - Astron
    Date created: 2019-04-04
    Description:  Business logic for ATDB. These functions are called from the views (views.py).
"""
import time
import datetime
import logging
from .common import timeit
from ..models import Observation, DataProduct, TimeUsed

DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%Y-%m-%d %H:%M:%SZ"
DJANGO_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

logger = logging.getLogger(__name__)

def get_delta_in_minutes(timestamp1,timestamp2):
    """
    return minutes between 2 dates
    :param timestamp1:
    :param timestamp2:
    :return:
    """
    date1 = datetime.datetime.strptime(timestamp1, TIME_FORMAT)
    date2 = datetime.datetime.strptime(timestamp2, TIME_FORMAT)
    minutes = (date2 - date1).total_seconds() / 60.0
    return int(minutes)

def get_minutes_left(target_time):
    """
    Determine how many minutes is left until the target_time is reached
    :param target_timestring: The target time defined as string "YYYY-MM-DDThh:mm:ss"
    :return: minutes left
             A negative number of minutes means that the current time has already reached the target_time,
             there is no time left.
              A positive number of minutes means that there is still time left
    """
    now = datetime.datetime.utcnow()

    # convert the time to the same formats
    target_time = datetime.datetime.strptime(target_time.strftime('%Y-%m-%d %H:%M:%SZ'), TIME_FORMAT)

    # Convert to Unix timestamp
    d1_ts = time.mktime(now.timetuple())
    d2_ts = time.mktime(target_time.timetuple())
    minutes_left = round(((d2_ts - d1_ts) / 60), 2)

    return minutes_left

@timeit
def get_next_taskid(timestamp, taskid_postfix):
    """
    get the observation with a starttime closest to now.
    example url to directly access this function from the REST API:
    /atdb/get_next_observation?my_status=scheduled&observing_mode=imaging
    :param timestamp: timestamp on which the taskid is based
    :param taskid_postfix: optional addition to the tasked, like 190405001_IMG
    :return: taskid
    """

    logger.info("get_next_taskid("+timestamp+")")

    count = 0
    while True:
        count += 1
        taskid = timestamp + str(count).zfill(3)  # 20180606001
        taskid = taskid[2:]  # 180606001

        # add an optional postfix, can be used to make pretty pipeline taskis' like 180905001_CAL
        if taskid_postfix != None:
            taskid = taskid + taskid_postfix  # 180606001_RAW

        # check if this taskid already exists. If it does, increase the counter and try again
        logger.info('checking taskid ' + str(taskid) + '..')
        found = Observation.objects.filter(taskID=taskid).count()

        if found==0:
            return taskid

    return -1


@timeit
def get_next_observation(my_status, observing_mode, datawriter):
    """
    get the observation with a starttime closest to now
    example url to directly access this function from the REST API:
    /atdb/get_next_observation?my_status=scheduled&observing_mode=imaging
    :param my_status: status to search for (probably 'scheduled'
    :param observing_mode: imaging or arts
    :param datawriter: wcudata1 or wcudata2 (to be implemented later)
    :return:
    """

    logger.info("get_next_observation("+my_status+','+observing_mode+','+str(datawriter)+")")

    # query the Observations table
    # disregard the observations which starttime are more than 1 minute in the past
    # nowisch = datetime.datetime.utcnow() - datetime.timedelta(minutes=1)
    try:
        # observations = Observation.objects.filter(my_status=my_status).filter(observing_mode__icontains=observing_mode)
        # nv: this should be all to enable datawriter2, but test first before activating
        observations = Observation.objects.filter(my_status=my_status).filter(observing_mode__icontains=observing_mode).filter(data_location__icontains=datawriter)

        # extract the earliest observation and its relevant properties to return to the view
        earliest_observation = observations.earliest('starttime')
        taskID = earliest_observation.taskID
        minutes_left = get_minutes_left(earliest_observation.starttime)
    except:
        taskID = None
        minutes_left = None

    return taskID,minutes_left


# /atdb/post_dataproducts?taskid=190405001
@timeit
def add_dataproducts(taskID, dataproducts):
    """
    :param taskID: taskid of the observation
    :param dataproducts: json list of dataproducts to be added to the provided taskid
    :return:
    """
    number_of_dataproducts = len(dataproducts)
    logger.info("add_dataproducts("+taskID+','+str(number_of_dataproducts)+")")

    # get the common fields from the observation based on the given taskid
    parent = Observation.objects.get(taskID=taskID)
    irods_collection = parent.irods_collection
    parent_data_location = parent.data_location

    for dp in dataproducts:
        node=dp.get('node',None)
        new_status = dp.get('new_status','defined')
        data_location = dp.get('data_dir',parent_data_location)
        size = dp.get('size', 0)
        myDataProduct = DataProduct(taskID=taskID,
                                    node=node,
                                    data_location=data_location,
                                    filename=dp['filename'],
                                    name=dp['filename'],
                                    description=dp['filename'],
                                    task_type='dataproduct',
                                    new_status=new_status,
                                    parent=parent,
                                    irods_collection=irods_collection,
                                    size=size
                                    )

        logger.info('addding dataproduct: '+str(myDataProduct))
        myDataProduct.save()


@timeit
def get_time_used_data(param_to, param_from, report_type):
    """
    Structure the data in a json format as expected by the EnergyView frontend
    For compatibility reasons I use the same format as the Qurrent Qservice

    :param param_to: upper timestamp limit (to)
    :param param_from: lower timestamp limit (from)
    :param report_type:
    :return: data[], json structure with structured and calculated data
    """
    logger.info('get_time_used_data(' + param_to + ',' + param_from + ',' + report_type + ')')


    if report_type == 'time_used':
        data = get_time_used_data_version1(param_to,param_from,report_type)

    elif report_type == 'time_used_version1' or report_type == 'time_used_details':
        data = get_time_used_data_version1(param_to,param_from,report_type)

    elif report_type == 'time_used_system_pie' or report_type == 'system_pie':
        data = get_time_used_data_system_pie(param_to,param_from,report_type)

    elif report_type == 'time_used_science_pie' or report_type == 'science_pie':
        data = get_time_used_data_science_pie(param_to,param_from,report_type)

    return data


@timeit
def get_time_used_data_version1(param_to, param_from, report_type):
    """
    Structure the data in a json format as expected by the EnergyView frontend
    For compatibility reasons I use the same format as the Qurrent Qservice

    :param param_to: upper timestamp limit (to)
    :param param_from: lower timestamp limit (from)
    :param report_type:
    :return: data[], json structure with structured and calculated data
    """
    logger.info('get_time_used_data(' + param_to + ',' + param_from + ',' + report_type + ')')

    # initialize
    data = {}

    try:
        # accept format: ?from=2018-11-01&to=2018-11-08
        timestamp_from = datetime.datetime.strptime(param_from, DATE_FORMAT)
        timestamp_to = datetime.datetime.strptime(param_to, DATE_FORMAT)
    except:
        # accept format: ?from=2018-11-01T00:00:00Z&to=2018-11-08T00:00:00Z
        timestamp_from = datetime.datetime.strptime(param_from, DJANGO_TIME_FORMAT)
        timestamp_to = datetime.datetime.strptime(param_to, DJANGO_TIME_FORMAT)

    # calculate the total duration in minutes based on the from/to arguments of the query
    total_minutes = (timestamp_to - timestamp_from).total_seconds() / 60.0
    data['total_minutes'] = total_minutes

    # get all the observations in the given time range
    observations = TimeUsed.objects.filter(starttime__gte=timestamp_from, endtime__lte=timestamp_to)
    number_of_observations = len(observations)

    # loop through the observations and gather the time-on-sky per observing_mode.
    time_on_sky_details = {}
    time_on_sky_overview = {}

    was_archived = {}
    count_details = {}
    system_count = 0
    system_minutes = 0
    imaging_was_archived_minutes = 0
    imaging_was_archived_count = 0
    imaging_was_archived_size = 0
    arts_was_archived_minutes = 0
    arts_was_archived_count = 0
    arts_was_archived_size = 0

    for observation in observations:
        # taskID = observation.taskID

        duration = observation.duration / 60
        observing_mode = observation.observing_mode
        status = observation.my_status
        size = observation.size

        timestamp_archived = observation.timestamp_archived_derived

        # is_filler = not observation.skip_auto_ingest

        is_system_observation = False
        if 'POINTING' in observing_mode.upper():
            is_system_observation = True

        # count
        try:
            i = count_details.get(observing_mode + '@' + status)
            i = i + 1
        except:
            i = 1

        # aggregate duration
        try:
            observing_mode_duration = time_on_sky_details.get(observing_mode + '@' + status)
            observing_mode_duration += duration
            observing_mode_size += size
        except:
            observing_mode_duration = duration
            observing_mode_size = size

        # add specifics per status
        count_details[observing_mode + '@' + status] = i
        time_on_sky_details[observing_mode + '@' + status] = int(observing_mode_duration)

        # add some totals
        if is_system_observation:
            system_minutes = system_minutes + duration
            system_count = system_count + 1
        else:
            # only account (archived) observations that are not system observations
            if 'IMAGING' in observing_mode.upper() and timestamp_archived != None:
                imaging_was_archived_minutes = imaging_was_archived_minutes + duration
                imaging_was_archived_size = imaging_was_archived_size + size
                imaging_was_archived_count = imaging_was_archived_count + 1

            # add some overview
            if 'ARTS' in observing_mode.upper() and timestamp_archived != None:
                arts_was_archived_minutes = arts_was_archived_minutes + duration
                arts_was_archived_size = arts_was_archived_size + size
                arts_was_archived_count = arts_was_archived_count + 1

    # add some overview for archived observations
    was_archived['imaging_count'] = int(imaging_was_archived_count)
    was_archived['imaging_minutes'] = int(imaging_was_archived_minutes)
    was_archived['imaging_size'] = int(imaging_was_archived_size)
    was_archived['arts_count'] = int(arts_was_archived_count)
    was_archived['arts_minutes'] = int(arts_was_archived_minutes)
    was_archived['arts_size'] = int(arts_was_archived_size)
    was_archived['system_count'] = int(system_count)
    was_archived['system_minutes'] = int(system_minutes)

    time_on_sky_overview['was_archived'] = was_archived

    data['report_type'] = report_type
    data['total_count'] = number_of_observations
    data['count-details'] = count_details
    data['time-on-sky-overview'] = time_on_sky_overview
    data['time-on-sky-details'] = time_on_sky_details

    return data


@timeit
def get_time_used_data_system_pie(param_to, param_from, report_type):

    """
    # calld by: atdb/time-used?from=2018-11-23&to=2018-12-31&report_type=time_used_version2

    Gather the data from ATDB to feed the following query:

    System Pie
    - available time (time range from query)
      - check all that were ever RUNNING
        - science (all that has been ARCHIVED AND process_type has 'science_')
        - system (process_type has 'system_')
        - aborted (was aborted?) = lost time
        - idle (available - sum(all))

    "time_used_data": {
        "total_minutes": 41760.0,
        "system_minutes": 20439,
        "science_minutes": 1620,
        "report_type": "time_used_system_pie",
        "idle_minutes": 19617,
        "aborted_minutes": 84
    }

    :param param_to: upper timestamp limit (to)
    :param param_from: lower timestamp limit (from)
    :param report_type:
    :return: data[], json structure with structured and calculated data
    """
    logger.info('get_time_used_data(' + param_to + ',' + param_from + ',' + report_type + ')')

    # initialize
    data = {}
    system_minutes = 0
    science_minutes = 0
    aborted_minutes = 0

    try:
        # accept format: ?from=2018-11-01&to=2018-11-08
        timestamp_from = datetime.datetime.strptime(param_from, DATE_FORMAT)
        timestamp_to = datetime.datetime.strptime(param_to, DATE_FORMAT)
    except:
        # accept format: ?from=2018-11-01T00:00:00Z&to=2018-11-08T00:00:00Z
        timestamp_from = datetime.datetime.strptime(param_from, DJANGO_TIME_FORMAT)
        timestamp_to = datetime.datetime.strptime(param_to, DJANGO_TIME_FORMAT)

    # calculate the total duration in minutes based on the from/to arguments of the query
    total_minutes = (timestamp_to - timestamp_from).total_seconds() / 60.0
    data['total_minutes'] = total_minutes

    # get all the observations in the given time range
    observations = TimeUsed.objects.filter(starttime__gte=timestamp_from, endtime__lte=timestamp_to)

    # iterate over the queryset of observations
    for observation in observations:

        # only take into account observations that have been running
        if observation.timestamp_running_derived != None:

            duration = observation.duration / 60
            process_type = observation.process_type
            # declare all the old observations as system observations
            if process_type=='observation':
                process_type='system_observation'

            timestamp_archived = observation.timestamp_archived_derived
            timestamp_aborted = observation.timestamp_aborted_derived

            # classify this observation for the pie chart
            is_system_observation = not observation.science_observation
            is_science_observation = (observation.science_observation and timestamp_archived!=None)

            is_aborted = False
            if timestamp_aborted!= None:
                is_aborted = True


            # aggregate durations based on the classification
            if is_science_observation and not is_aborted:
                try:
                    science_minutes += duration
                except:
                    science_minutes = duration

            elif is_system_observation and not is_aborted:
                try:
                    system_minutes += duration
                except:
                    system_minutes = duration

            elif is_aborted:
                try:
                    aborted_minutes += duration
                except:
                    aborted_minutes = duration


    idle_minutes = total_minutes - (science_minutes + system_minutes + aborted_minutes)

    data['report_type'] = report_type
    data['total_minutes'] = total_minutes
    data['science_minutes'] = round(science_minutes)
    data['system_minutes'] = round(system_minutes)
    data['aborted_minutes'] = round(aborted_minutes)
    data['idle_minutes'] = round(idle_minutes)

    return data


@timeit
def get_time_used_data_science_pie(param_to, param_from, report_type):

    """
    # calld by: atdb/time-used?from=2018-11-23&to=2018-12-31&report_type=time_used_version2

    Gather the data from ATDB to feed the following queriy:

    Science Pie
    -  time range (from query)
       - check all that were ever ARCHIVED AND (process_type has 'science_')
         - Imaging/ARTS (first ring)
           - imaging: survey, argo, driftscan, filler_imaging (not in imaging time)?
           - arts: sc1, sc4, filler_sc1 (not in arts time), filler_sc4 (not in arts time)

    "time_used_data": {
        "report_type": "time_used_science_pie"
        "total_minutes": 41760.0,

        "Imaging" : {
           survey :
           argo   :
           drift  :
           filler :
        },
        "ARTS" : {
           SC1 :
           SC4 :
           filler_sc1 :
           filler_sc4 :
        }
    }

    :param param_to: upper timestamp limit (to)
    :param param_from: lower timestamp limit (from)
    :param report_type:
    :return: data[], json structure with structured and calculated data
    """
    logger.info('get_time_used_data(' + param_to + ',' + param_from + ',' + report_type + ')')

    try:
        # accept format: ?from=2018-11-01&to=2018-11-08
        timestamp_from = datetime.datetime.strptime(param_from, DATE_FORMAT)
        timestamp_to = datetime.datetime.strptime(param_to, DATE_FORMAT)
    except:
        # accept format: ?from=2018-11-01T00:00:00Z&to=2018-11-08T00:00:00Z
        timestamp_from = datetime.datetime.strptime(param_from, DJANGO_TIME_FORMAT)
        timestamp_to = datetime.datetime.strptime(param_to, DJANGO_TIME_FORMAT)

    # initialize
    data = {}
    imaging_data = {}
    arts_data = {}
    
    imaging_argo_duration = 0
    imaging_argo_size = 0

    imaging_drift_duration = 0
    imaging_drift_size = 0

    imaging_filler_duration = 0
    imaging_filler_size = 0

    imaging_survey_duration = 0
    imaging_survey_size = 0

    arts_sc1_duration = 0
    arts_sc1_size = 0

    arts_sc4_duration = 0
    arts_sc4_size = 0

    arts_sc1_filler_duration = 0
    arts_sc1_filler_size = 0

    arts_sc1_drift_duration = 0
    arts_sc1_drift_size = 0

    arts_sc4_filler_duration = 0
    arts_sc4_filler_size = 0

    arts_sc4_drift_duration = 0
    arts_sc4_drift_size = 0

    # calculate the total duration in minutes based on the from/to arguments of the query
    total_minutes = (timestamp_to - timestamp_from).total_seconds() / 60.0
    data['total_minutes'] = total_minutes

    # get all the observations in the given time range
    observations = TimeUsed.objects.filter(starttime__gte=timestamp_from, endtime__lte=timestamp_to).filter(science_observation=True)

    # iterate over the queryset of observations
    for observation in observations:

        # only take into account observations that have been running
        if observation.timestamp_archived_derived != None:

            duration = observation.duration / 60
            observing_mode = observation.observing_mode
            process_type = observation.process_type
            is_filler = observation.filler
            name = observation.name
            status = observation.my_status
            size = observation.size

            # classify this observation for the pie chart
            # aggregate durations and sizes based on the classification

            if 'IMAGING' in observing_mode.upper():
                if 'ARGO' in name.upper():
                    imaging_argo_duration = imaging_argo_duration + duration
                    imaging_argo_size = imaging_argo_size + size

                if 'DRIFT' in name.upper() or 'DRIFT' in process_type.upper():
                    imaging_drift_duration = imaging_drift_duration + duration
                    imaging_drift_size = imaging_drift_size + size

                if is_filler:
                    imaging_filler_duration = imaging_filler_duration + duration
                    imaging_filler_size = imaging_filler_size + size

                # it is always survey, regardless of the above properties?
                imaging_survey_duration = imaging_survey_duration + duration
                imaging_survey_size = imaging_survey_size + size

            # add some overview
            if 'ARTS' in observing_mode.upper():

                if 'SC1' in observing_mode.upper():
                    arts_sc1_duration = arts_sc1_duration + duration
                    arts_sc1_size = arts_sc1_size + size

                    if is_filler:
                        arts_sc1_filler_duration = arts_sc1_filler_duration + duration
                        arts_sc1_filler_size = arts_sc1_filler_size + size

                    if 'DRIFT' in name.upper() or 'DRIFT' in process_type.upper():
                        arts_sc1_drift_duration = arts_sc1_drift_duration + duration
                        arts_sc1_drift_size = arts_sc1_drift_size + size
                    
                if 'SC4' in observing_mode.upper():
                    arts_sc4_duration = arts_sc4_duration + duration
                    arts_sc4_size = arts_sc4_size + size

                    if is_filler:
                        arts_sc4_filler_duration = arts_sc4_filler_duration + duration
                        arts_sc4_filler_size = arts_sc4_filler_size + size

                    if 'DRIFT' in name.upper() or 'DRIFT' in process_type.upper():
                        arts_sc4_drift_duration = arts_sc4_drift_duration + duration
                        arts_sc4_drift_size = arts_sc4_drift_size + size

    imaging_data['survey_minutes'] = round(imaging_survey_duration)
    imaging_data['survey_bytes'] = round(imaging_survey_size)
    imaging_data['argo_minutes'] = round(imaging_argo_duration)
    imaging_data['argo_bytes'] = round(imaging_argo_size)
    imaging_data['drift_minutes'] = round(imaging_drift_duration)
    imaging_data['drift_bytes'] = round(imaging_drift_size)
    imaging_data['filler_minutes'] = round(imaging_filler_duration)
    imaging_data['filler_bytes'] = round(imaging_filler_size)

    arts_data['sc1_minutes'] = round(arts_sc1_duration)
    arts_data['sc1_bytes'] = round(arts_sc1_size)
    arts_data['sc4_minutes'] = round(arts_sc4_duration)
    arts_data['sc4_bytes'] = round(arts_sc4_size)

    arts_data['sc1_filler_minutes'] = round(arts_sc1_filler_duration)
    arts_data['sc1_filler_bytes'] = round(arts_sc1_filler_size)
    arts_data['sc1_drift_minutes'] = round(arts_sc1_drift_duration)
    arts_data['sc1_drift_bytes'] = round(arts_sc1_drift_size)

    arts_data['sc4_filler_minutes'] = round(arts_sc4_filler_duration)
    arts_data['sc4_filler_bytes'] = round(arts_sc4_filler_size)
    arts_data['sc4_drift_minutes'] = round(arts_sc4_drift_duration)
    arts_data['sc4_drift_bytes'] = round(arts_sc4_drift_size)

    data['report_type'] = report_type
    data['total_minutes'] = total_minutes
    data['imaging'] = imaging_data
    data['arts'] = arts_data

    return data


@timeit
def mark_period_as(param_from, param_to, taskid_from, taskid_to, period_type, quality, observing_mode):
    """
    Structure the data in a json format as expected by the EnergyView frontend
    For compatibility reasons I use the same format as the Qurrent Qservice

    /atdb/mark-period-as?from=2019-07-23T00:00:00Z&to=2019-07-23T11:00:00Z&type=system

    :param param_to: upper timestamp limit (to)
    :param param_from: lower timestamp limit (from)
    :param type:
    :return: number of changed observations in a json response

    """
    if period_type!=None:
        logger.info('mark_period_as(' + str(param_from) + ',' + str(param_to) + ','
                    + str(taskid_from) + ',' + str(taskid_to) +',' + str(period_type) + ')')

    if quality!=None:
        logger.info('mark_period_as(' + str(param_from) + ',' + str(param_to) + ','
                    + str(taskid_from) + ',' + str(taskid_to) +',' + str(quality) + ')')

    # first try the range of taskid's...
    if taskid_from!=None and taskid_to!=None:
        observations = Observation.objects.filter(taskID__gte=taskid_from, taskID__lte=taskid_to)
    else:
    # ... then try the range of timestamps
        try:
            # accept format: ?from=2018-11-01&to=2018-11-08
            timestamp_from = datetime.datetime.strptime(param_from, DATE_FORMAT)
            timestamp_to = datetime.datetime.strptime(param_to, DATE_FORMAT)
        except:
            # accept format: ?from=2018-11-01T00:00:00Z&to=2018-11-08T00:00:00Z
            timestamp_from = datetime.datetime.strptime(param_from, DJANGO_TIME_FORMAT)
            timestamp_to = datetime.datetime.strptime(param_to, DJANGO_TIME_FORMAT)

        # get all the observations in the given time range
        observations = Observation.objects.filter(starttime__gte=timestamp_from, endtime__lte=timestamp_to)

    if observing_mode!=None:
        observations = observations.filter(observing_mode__icontains=observing_mode)

    changed_observations = 0

    number_of_observations = observations.count()
    count = 0

    for observation in observations:
        count = count + 1
        logger.info('handling observation '+str(count)+ ' of '+ str(number_of_observations))
        changed = False

        # first check the mark-period-as a 'period_type' functionality
        if period_type!=None:
            if 'science' in period_type:
                if not observation.science_observation:
                    observation.science_observation = True
                    changed = True

            if 'system' in period_type:
                if observation.science_observation:
                    observation.science_observation = False
                    changed = True

            if 'filler' in period_type:
                if not observation.filler:
                    observation.filler = True
                    changed = True

            if not 'filler' in period_type:
                if observation.filler:
                    observation.filler = False
                    changed = True

            if 'status' in period_type:
                # status_aborted => aborted
                _,new_status = period_type.split('_')
                observation.new_status=new_status
                changed = True

        # then check the mark-period-as a 'quality' functionality
        if quality!=None:
            if observation.quality!=quality:

                #logger.info(str(count)+ ') sending to ALTA: ' + str(observation.taskID) + ' = ' + quality)
                # result = send_quality_to_alta(observation.taskID,quality)
                # if result=='OK':

                observation.quality=quality
                changed = True

        if changed:
            logger.info(str(count) + ') observation has changed, save it')
            changed_observations = changed_observations + 1
            observation.save()

    logger.info('mark_period_as() : finished')

    return changed_observations

@timeit
def send_quality_to_alta(taskID, quality):
    """
    Send 'good' or 'bad' to ALTA, setting it to 'bad' will result in deleting from irods.
    :param taskID:
    :return:
    """
    # later read all these settings from the (to be created) configuration table
    # user credentials possible from a secret file like how it is done on ALTA.
    # /etc/atdb.secret

    # make sure to have the alta_interface package installed on the server
    # login to webservver
    # cd /var/www/atdb.astron.nl/atdb
    # source .env/bin/activate
    # pip install https://support.astron.nl/nexus/content/repositories/snapshots/nl/astron/alta/ALTA_interface_lib/ALTA_interface_lib-1.0.0-dev-20180925_011101.tar.gz --upgrade

    logger.info('send_quality_to_alta(' + str(taskID) + ',' + str(quality) + ')')

    # connect ALTA
    try:
        from alta_interface.alta_interface import ALTA

        # read the credentials from the settings
        from django.conf import settings
        alta_host = settings.ALTA_HOST
        alta_user = settings.ALTA_USER
        alta_pass = settings.ALTA_PASS
        alta = ALTA(alta_host, alta_user, alta_pass)

        alta.do_PUT('activities:quality',None,quality,taskID)
        check_quality = alta.do_GET('activities:quality', None, taskID, None)
        if check_quality == quality:
            logger.info('check_quality = OK ')
            return "OK"
        else:
            return "NOT_OK"
    except Exception as error:
        try:
            message = str(error.message)
            logger.error(message)
            return message
        except:
            return str(error)

    logger.info('send_quality_to_alta() : finished')

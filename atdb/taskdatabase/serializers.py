from rest_framework import serializers
from .models import DataProduct, Observation, Status, TaskObject, Times
import logging

logger = logging.getLogger(__name__)

class StatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Status
        fields = ('id','name','timestamp','property_taskID','property_task_type')


class DataProductSerializer(serializers.ModelSerializer):
    # this adds a 'parent_observation' list with hyperlinks to the DataProduct API.
    # note that 'generatedByObservation' is not defined in the DataProduct model, but in the Observation model.

    parent = serializers.PrimaryKeyRelatedField(
        many=False,
        queryset=Observation.objects.all(),
        required=False,
    )

    status_history = serializers.StringRelatedField(
        many=True,
        required=False,
    )

    class Meta:
        model = DataProduct
        fields = ('id','task_type','name','filename','description',
                  'taskID','creationTime','size','quality',
                  'my_status','new_status','status_history','parent',
                  'data_location','irods_collection','node')


class ObservationSerializer(serializers.ModelSerializer):
    # this adds a 'generated_dataproducts' list with hyperlinks to the Observation API.
    # note that 'generated_dataproducts' is not defined in the DataProduct model, but comes from the related_field in Observation.parent.

    generated_dataproducts = serializers.StringRelatedField(
        many=True,
        required=False,
    )

    status_history = serializers.StringRelatedField(
        many=True,
        required=False,
    )

    class Meta:
        model = Observation
        fields = ('id','task_type', 'name', 'process_type','taskID','beamPattern',
                  'field_name','field_ra','field_ha','field_dec','field_beam','integration_factor','central_frequency',
                  'creationTime','starttime','endtime', 'duration', 'size',
                  'my_status','new_status','status_history',
                  'generated_dataproducts','telescopes',
                  'data_location', 'irods_collection','node','control_parameters',
                  'skip_auto_ingest','observing_mode','science_mode','parset_location',
                  'par_file_name','number_of_bins','start_band','end_band','process_triggers','beams', 'delay_center_offset',
                  'locality_policy','max_lifetime_on_disk','quality','science_observation','filler','ingest_progress',
                  'timestamp_starting','timestamp_running','timestamp_completing',
                  'timestamp_ingesting','timestamp_archived','timestamp_aborted','timestamp_ingest_error')

class TimesSerializer(serializers.ModelSerializer):
    #readonly = True
    class Meta:
        model = Times
        fields = ('taskID','observing_mode','starttime','endtime',
                  'duration','total_size','write_speed',
                  'timestamp_ingesting_derived','timestamp_ingest_error_derived','ingest_duration','ingest_speed')
        #read_only_fields = fields

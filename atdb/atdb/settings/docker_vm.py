from atdb.settings.base import *

# Import production setting must remain False.
DEBUG = True

ALLOWED_HOSTS = ["*"]


#####################################################
# These settings mainly deal with https.
# See http://django-secure.readthedocs.io/en/latest/middleware.html
# Check the warning and instructions with:
# (.env) atdb@/var/.../atdb ./manage.py check --deploy --settings=atdb.settings.prod
#####################################################
# Assume SSL is correctly set up.
SSL_ENABLED = False
if SSL_ENABLED:
    # True: Django now checks that cookies are ONLY sent over SSL.
    # https://docs.djangoproject.com/en/1.11/ref/settings/#session-cookie-secure
    SESSION_COOKIE_SECURE = True
    # True: Django now checks that csrf tokens are ONLY sent over SSL.
    # https://docs.djangoproject.com/en/1.11/ref/settings/#csrf-cookie-secure
    CSRF_COOKIE_SECURE = True
    # True: Always redirect requests back to https (currently ignored as Nginx should enforces https).
    #       Alternatively, enable and add set SECURE_PROXY_SSL_HEADER.
    SECURE_SSL_REDIRECT = False
    # Setting this to a non-zero value, will default the client UA always to connect over https.
    # Unclear how or if this possibly affects other *.astron.nl domains. Especially, if these do
    # not support https whether this option then breaks those http-only locations.
    # SECURE_HSTS_SECONDS = 31536000

# True: Enables a header that disables the UA from 'clever' automatic mime type sniffing.
# http://django-secure.readthedocs.io/en/latest/settings.html#secure-content-type-nosniff
# https://stackoverflow.com/questions/18337630/what-is-x-content-type-options-nosniff
SECURE_CONTENT_TYPE_NOSNIFF = True

# True: Enables a header that tells the UA to switch on the XSS filter.
# http://django-secure.readthedocs.io/en/latest/middleware.html#x-xss-protection-1-mode-block
SECURE_BROWSER_XSS_FILTER = True

# Prevents the site from being deployed within a iframe.
# This prevent click-jacking attacks.
# See; https://docs.djangoproject.com/en/1.11/ref/clickjacking/
X_FRAME_OPTIONS = 'DENY'
#####################################################

DATABASES = {
    'default': {
         'ENGINE': 'django.db.backends.postgresql_psycopg2',
         'USER': 'dbadmin',
         'PASSWORD': 'dbadmin123',

         # database runs locally in postgres
         'NAME': 'atdb',
         'HOST': '192.168.22.30',
         'PORT': '5433',

         # database runs on a virtual machine
         # 'HOST': 'alta-sys-db.astron.nl',
         # 'PORT': '5432',
         # 'NAME': 'altadb'
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = []

ALTA_HOST = "http://192.168.22.30/altapi"
ALTA_USER = "atdb_write"
ALTA_PASS = "7VVJruVn8W1n"
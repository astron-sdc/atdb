#!/bin/bash
##---------------------------------------------------------------------------------------------------------------------#
##! \brief   build_atdb_from_svn.sh (version 21 mar 2019, Nico Vermaas)
##!          Description: This script checks out ATDB from svn and creates a tar artifact for upload to Nexus
##!          In:  $1  Version (1.1.16)
##!          In:  $2  Branch (branches/atdb-branch-sprint-109, trunk)
##!          In:  $3  User (vermaas, jenkins, ...)
##!          Out: None
##!          Returns: None
##!          Example: ./build_atdb_from_svn.sh 1.1.16 branches/atdb-branch-sprint-109 vermaas
##---------------------------------------------------------------------------------------------------------------------#

VERSION=$1
if [ $# -lt 1 ]
    then
        VERSION=unknown-version
fi
echo "VERSION = $VERSION"

BRANCH=$2
if [ $# -lt 2 ]
    then
        BRANCH=trunk
        #BRANCH=branches/atdb-branch-sprint-109
fi
echo "BRANCH = $BRANCH"

USER=$3
if [ $# -lt 3 ]
    then
        USER=vermaas
fi
echo "USER = $USER"


#--- checkout from SVN - TODO: when Jenkins is used, omit this part. -------------------------------------
echo "SVN Checkout ATDB from $BRANCH"
USER=vermaas
svn checkout https://svn.astron.nl/apertif/atdb/$BRANCH atdb --username $USER

#----------------------------------------------------------------------------------------------------------
echo "Build a distribution for ATDB backend and atdb_services"

cd atdb
# omitting static files for now, because virtualenv/python3 is not installed on ALTA2 build host
#virtualenv -p python3 .env
#source .env/bin/activate
#python manage.py collectstatic --settings atdb.settings.dev

#--- build ATDB artifact ----------------------------------------------------------------------------------
echo "create ATDB artifact"
ARTIFACT_NAME="ATDB-"$VERSION".tar"
tar -cvf $ARTIFACT_NAME . --exclude="__pycache__" --exclude="*.log" --exclude="*.bat" --exclude=".env"

#-- upload ATDB artifcat to Nexus -------------------------------------------------------------------------
ARTIFACT_UPLOAD_BASE_PATH="https://support.astron.nl/nexus/content/repositories/snapshots/nl/astron/atdb/"
ARTIFACT_UPLOAD_PATH="${ARTIFACT_UPLOAD_BASE_PATH}${ARTIFACT_NAME}"
ARTIFACT_BUILD_PATH="$(pwd)/${ARTIFACT_NAME}"

echo "Upload ${ARTIFACT_NAME} to $ARTIFACT_UPLOAD_PATH"
curl --insecure --upload-file ${ARTIFACT_BUILD_PATH} -u upload:upload ${ARTIFACT_UPLOAD_PATH}

mv $ARTIFACT_NAME ..
#cd ..

#---build and upload atdb_interface ------------------------------------------------------------------------
echo "Build atdb_interface package and upload to Nexus"
cd atdb_interface_pip
chmod +x *.sh
./build.sh
./upload_to_nexus.sh
cd ..

#---build and upload atdb_services ------------------------------------------------------------------------
echo "Build atdb_services package and upload to Nexus"
cd atdb_services_pip
chmod +x *.sh
./build.sh
./upload_to_nexus.sh
cd ../..


#rm -rf atdb

echo "Finished script for: VERSION = $VERSION, BRANCH = $BRANCH, USER = $USER,"

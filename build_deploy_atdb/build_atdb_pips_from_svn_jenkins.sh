#!/bin/bash
##---------------------------------------------------------------------------------------------------------------------#
##! \brief   build_atdb_pips_from_svn.sh (version 21 mar 2019, Nico Vermaas)
##!          Description: This script checks out the pip installable packages from svn and uploads them as
##!          tar.gz artifacts to Nexus.
##---------------------------------------------------------------------------------------------------------------------#

cd ..

#---build and upload atdb_interface ------------------------------------------------------------------------
echo "Build atdb_interface package and upload to Nexus"
cd atdb_interface_pip
chmod +x *.sh
./build.sh
./upload_to_nexus.sh
cd ..

#---build and upload atdb_services ------------------------------------------------------------------------
echo "Build atdb_services package and upload to Nexus"
cd atdb_services_pip
chmod +x *.sh
./build.sh
./upload_to_nexus.sh

cd ../..

echo "Done"

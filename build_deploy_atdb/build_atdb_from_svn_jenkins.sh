#!/bin/bash
##---------------------------------------------------------------------------------------------------------------------#
##! \brief   build_atdb_from_svn.sh (version 21 mar 2019, Nico Vermaas)
##!          Description: This script creates a tar artifact for upload to Nexus
##!          In:  $1  Version (1.1.16)
##---------------------------------------------------------------------------------------------------------------------#

VERSION=$1
if [ $# -lt 1 ]
    then
        VERSION=unknown
fi
echo "VERSION = $VERSION"

#----------------------------------------------------------------------------------------------------------
echo "Build a artifact for the ATDB backend "
cd ..

#--- build ATDB artifact ----------------------------------------------------------------------------------
echo "create ATDB artifact"
ARTIFACT_NAME="ATDB-"$VERSION".tar"
tar -cvf $ARTIFACT_NAME . --exclude="__pycache__" --exclude="*.log" --exclude="*.bat" --exclude=".env" --exclude=".svn"

#-- upload ATDB artifcat to Nexus -------------------------------------------------------------------------
ARTIFACT_UPLOAD_BASE_PATH="https://support.astron.nl/nexus/content/repositories/snapshots/nl/astron/atdb/"
ARTIFACT_UPLOAD_PATH="${ARTIFACT_UPLOAD_BASE_PATH}${ARTIFACT_NAME}"
ARTIFACT_BUILD_PATH="$(pwd)/${ARTIFACT_NAME}"

echo "Upload ${ARTIFACT_NAME} to $ARTIFACT_UPLOAD_PATH"
curl --insecure --upload-file ${ARTIFACT_BUILD_PATH} -u upload:upload ${ARTIFACT_UPLOAD_PATH}

mv $ARTIFACT_NAME ..

echo "Finished script for: VERSION = $VERSION"

# deploy script for ATDB
# Nico Vermaas - 18 march 2019
##---------------------------------------------------------------------------------------------------------------------#
##! \brief   Description: This script downloads an artifact from nexus and installs ATDB in the current directory
##!          In:  $1  Version (1.1.16)
##!          Out: None
##!          Returns: None
##!          Preconditions: be user atdb in directory /var/www/atdb.astron.nl
##!          Postconditions: None
##!          Notes:
##---------------------------------------------------------------------------------------------------------------------#

VERSION=$1
if [ $# -eq 0 ]
    then
        VERSION=unknown-version
fi

ARTIFACT_NAME="ATDB-"$VERSION".tar"

export SOURCE_DIR=/vagrant/repository
export DEST_DIR=/var/www/atdb.astron.nl

# download artifact from nexus
sudo chown atdb:webapps $DEST_DIR
cd $DEST_DIR
wget https://support.astron.nl/nexus/content/repositories/snapshots/nl/astron/atdb/$ARTIFACT_NAME

# make a backup of the current system
rm ATDB-previous.tar
tar -cvf ATDB-previous.tar atdb --exclude="__pycache__" --exclude="*.log" --exclude="*.bat"

# remove current version
rm -r $DEST_DIR/atdb/taskdatabase/
rm -r $DEST_DIR/atdb/urls.py

# untar and deploy ATDB ***
tar -xvf $ARTIFACT_NAME --exclude="__pycache__" --exclude="*.log" --exclude="*.bat" --exclude=base.py

# remove the deployed archive
rm $ARTIFACT_NAME

# run as user atdb in virtualenv
cd $DEST_DIR/atdb
source .env/bin/activate

# migrate the database (no makemigrations, because the migration files should be up-to-date in the tar package
python manage.py makemigrations taskdatabase --settings atdb.settings.prod
python manage.py migrate --settings atdb.settings.prod

# restart supervisor and ATDB
sudo supervisorctl restart atdb
cd ..
echo Congratulations! You may have just survived another ATDB deploy!

atdb_interface
==============
This module contains a service that run standalone and interact with the Apertif Task Database REST API.

See 'atdb_interface -h' for help and 'atdb_interface -e' for examples.

This package is required when using atdb_services

Installing
^^^^^^^^^^
To install a version from Nexus repository use::

    > pip install <<nexus url>> --upgrade
    or download and install the tarball,
    > pip install atdb_interface.tar.gz --upgrade

Within the development environment such as with PyCharm one can install the package within the virtualenv with which
PyCharm is configured. To avoid uninstall and install after each code change pip can install packages within development
mode::

    (.env) > pip install -e ..project../atdb_interface_pip --upgrade

This will install the package with soft links to the original code, such that one gets immediate refresh within PyCharm,
which is used for refactoring, code completion, imports etc.

Uninstalling
^^^^^^^^^^^^
Uninstall is trivial using the command (watch out for the '-')::

    > pip uninstall atdb-interface

or without confirmation::

    > pip uninstall --yes atdb-interface
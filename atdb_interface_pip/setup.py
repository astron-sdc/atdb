from setuptools import setup, find_packages

def readme():
    with open('README.rst') as f:
        return f.read()

setup(name='atdb_interface',
      version='1.2.9',
      description='ATDB interface',
      url='https://www.astron.nl/wsrt/wiki/doku.php?id=atdb:atdb_interface',
      author='Nico Vermaas - Astron',
      author_email='vermaas@astron.nl',
      license='BSD',
      install_requires=['requests'],
      packages=find_packages(),
      entry_points={
            'console_scripts': [
                  'atdb_interface=atdb_interface.atdb_interface:main',
            ],
      },
      )